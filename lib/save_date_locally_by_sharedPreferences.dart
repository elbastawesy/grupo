import 'package:shared_preferences/shared_preferences.dart';

class SaveDataLocally {
  setUserData({
    String phoneNumber,
    String avatar,
    String userName,
    String joinDate,
    String userId,
  }) async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    preferences.setBool('visited', true);
    preferences.setString('phoneNumber', phoneNumber);
    preferences.setString('avatar', avatar);
    preferences.setString('userName', userName);
    preferences.setString('joinDate', joinDate);
    preferences.setString('userId', userId);
    preferences.setInt('range', 1);
    preferences.setBool('allowNotifications', true);
  }

  Future<bool> checkVisited() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    final visited = pref.getBool('visited') ?? false;
    return visited;
  }

  Future<bool> allowNotifications() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    final allowNoti = pref.getBool('allowNotifications') ?? true;
    return allowNoti;
  }

  Future<String> getUserName() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    final userName = preferences.getString('userName') ?? '';
    return userName;
  }

  Future<String> getUserAvatar() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    final userAvatar = preferences.getString('avatar') ?? '';
    return userAvatar;
  }

  Future<String> getUserPhoneNumber() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    final userNumber = preferences.getString('phoneNumber') ?? '';
    return userNumber;
  }

  Future<String> getUserJOinDate() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    final joinDate = preferences.getString('joinDate') ?? '';
    return joinDate;
  }

  Future<String> getUserId() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    final userId = preferences.getString('userId') ?? '';
    return userId;
  }

  Future<String> getUserBackendId() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    final userId = preferences.getString('userBackendId') ?? '';
    return userId;
  }

  Future<int> getRangeOfMessages() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    final rangeOfMessages = preferences.getInt('range') ?? 1;
    return rangeOfMessages;
  }

  removeUserData() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    await preferences.clear();
  }
}
