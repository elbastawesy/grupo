// Privacy policy content
const String INTRODUCTION =
    'Grupoza Inc. (“ Grupoza ,” “ we ,” “ us ”) is committed to protecting your privacy. This Privacy Policy applies to our website, online service and our Grupo mobile application (“ Grupo ”) (collectively, the “ Service ”). This Privacy Policy applies only to the practices of companies we own, control, or are united with under common control. By accessing or using the Service, you signify that you have read, understood and agree to our collection, storage, use and disclosure of your personal information as described in this Privacy Policy.';

// INFORMATION WE COLLECT
const String INFORMATION_WE_COLLECT =
    'Generally, we collect personal information from our users in order to provide you with a personalized, useful and efficient experience. The categories of information we collect can include:';

const String INFORMATION_WE_COLLECT_1 =
    ' Information You Provide . You must register in order to use Grupo and access most of the features of the Service, however there are certain activities, such as browsing our website that you may do without providing any personal information.';

const String INFORMATION_WE_COLLECT_1_1 =
    '- Account Information . When you first launch Grupo, we assign you a user id if you didn’t specify one but we do not require you to provide your name or email address. In order to activate Grupo, we ask you to provide and verify your telephone number.';

const String INFORMATION_WE_COLLECT_1_2 =
    '- Additional Information . Grupo members may also choose to provide additional information which may be shared with other Grupo users or through your public profile, such as your name, location, photograph and other information relating your use of the Service. If you email us, we may keep your message, email address and contact information to respond to your request. If you connect authenticate on the Service using your account on another service or connect your account on another service to your Grupo account in order to cross-post between the Service and that service, the other service may send us your registration or profile information on that service and other information that you authorize. This information helps us to maintain the security of the Service, helps us improve the Service, and is deleted from the Service within a few weeks of your disconnecting from our Service your account on the other service. Providing the additional information described in this section is entirely optional.';

const String INFORMATION_WE_COLLECT_1_END =
    'Depending on your settings, some of this personal information may be displayed on your public profile and viewable to others on the Service.';

const String INFORMATION_WE_COLLECT_2 =
    'Posts and Other Public Information . The Service is primarily designed to help you share information with the world through public posts. Most of the information you provide us through the Grupo is information you are asking us to make public. Your public information includes the messages you post; the metadata provided with each post; the language, country, and time zone associated with your account; and the posts you mark as likes or, and many other bits of information that result from your use of the Service. We may use this information to make inferences, such as inferences about your interests, and to customize the content we show you, including ads. Our default is almost always to make the information you provide through the Grupo public for as long as you do not delete it. Grupo broadly and instantly disseminates your public information to a wide range of users, customers, and services. When you share information or content like photos, videos, and links via the Service, you should think carefully about what you are making public.';

const String INFORMATION_WE_COLLECT_3 =
    'Links . We may keep track of how you interact with links across the Service, including third-party services and client applications, by redirecting clicks or through other means. We do this to help improve the Service, to provide more relevant advertising, and to be able to share aggregate click statistics such as how many times a particular link was clicked on.';

const String INFORMATION_WE_COLLECT_4 =
    'Cookies . Like many websites, we use cookies and similar technologies to collect additional website usage data and to improve the Service, but we do not require cookies for many parts of the Service.. A cookie is a small data file that is transferred to your computer or mobile device. Grupoza may use both session cookies and persistent cookies to better understand how you interact with the Service, to monitor aggregate usage by our users and web traffic routing on our Service, and to customize and improve the Service. Most Internet browsers automatically accept cookies. You can instruct your browser, by changing its settings, to stop accepting cookies or to prompt you before accepting a cookie from the websites you visit. However, some Services may not function properly if you disable cookies.';

const String INFORMATION_WE_COLLECT_5 =
    'Log Data. When you interact with the Service, for example when you visit our websites, sign into the Service, or interact with our email notifications, we may receive information (“ Log Data ”) such as your access time, device ID, application ID or other unique identifier; domain name; IP address; the referring web page; pages visited; sessions and session lengths; langua e information; device name and model; operating system information; and cookie information. We may also employ clear gifs (also known as web beacons) which are used to anonymously track your usage patterns when you use Grupo. In addition, we may use clear gifs in HTML-based emails sent to our users to track which emails are opened and which links are clicked by recipients. The information allows for more accurate reporting and improvement of the Service. We may also collect analytics data, or use third-party analytics tools, to help us measure traffic and usage trends of the Service. These tools collect information sent by your browser or mobile device, including the pages you visit, your use of third party applications, and other information that assists us in analyzing and improving the Service. We may receive reports from third party analytics providers, which we may combine with the personal data we collect from you directly. We may combine this information with other information that we have collected about you. If the any such combination of information would identify you as an individual, we will treat the combined information as personal information. Grupoza uses Log Data to provide, understand, and improve the Service, to make inferences about your interests, and to customize the content we show you, including ads. We will either delete Log Data or remove any common account identifiers, such as your username, full IP address, or email address, after a maximum of 36 months.';

const String INFORMATION_WE_COLLECT_6 =
    'Location Information . Grupoza may receive information about your location. For example, you may choose to publish your location in your posts to Grupo and in your Grupo profile. We may also determine location by using other data from your device, such as precise location information from GPS, information about wireless networks or cell towers near your mobile device, or your IP address. We may use and store information about your location to provide features of our Service, such as allowing to you connect with your local Grupo community. If you do not want location information collected through the Service, you may opt-out by changing the settings on your device. However, some features of the Service, such as the ability to share posts with Grupo users who are near you, may be unavailable if we are unable to receive your location information.';

const String INFORMATION_WE_COLLECT_6_END =
    'Social Media Sites . You can like us on Facebook, follow us on Twitter, and promote us on social media sites. We collect information about who has liked us or followed us. You may also share messages you post on Grupo through Facebook, Twitter, Instagram, and other social media accounts.';

const String INFORMATION_WE_COLLECT_7_1 =
    'Third-Parties and Affiliates . Grupoza uses a variety of third-party services to help provide the Service, such as hosting our various websites, and to help us understand and improve the use of our Service, such as Google Analytics and Clicky . These third-party service providers may collect information sent by your browser as part of a web page request, such as cookies or your IP address pr through your use of Grupo. Third-party ad partners may share information with us, like a browser cookie ID, website URL visited, mobile device ID, or cryptographic hash of a common account identifier, to help us measure and tailor ads. For example, this allows us to display ads about things you may have already shown interest in off of the Service. We may also receive information about you from our corporate affiliates in order to help provide, understand, and improve our Services and our affiliates’ services, including the delivery of ads.';

const String INFORMATION_WE_COLLECT_7_2 =
    'If you do not want to receive the benefits of targeted advertising, you may opt out of some network advertising programs that use your information by visiting the NAI Opt-Out Page . Please note that even if you choose to remove your information (opt out), you will still see advertisements while you\'re browsing online. However the advertisements you see may be less relevant to you and your interests. Additionally, many network advertising programs allow you to view and manage the interest categories they have compiled from your online browsing activities. These interest categories help determine the types of targeted advertisements you may receive. The NAI Opt-Out Page provides a tool that identifies its member companies that have cookies on your browser and provides a mechanism to opt out of receiving cookies from those companies. Please note that if you opt-out of targeted advertising, we may still track your visits to the Platforms for our own analytics, operations and security purposes.';

const String INFORMATION_WE_COLLECT_7_END =
    'We use this information to operate, maintain, and provide to you the features and functionality of Grupo, as well as to communicate directly with you and permit you to share your activities on the Service with our contacts and other users.';

// SHARING PERSONAL INFORMATION WITH THIRD PARTIES
const String SHARING_PERSONAL_INFORMATION =
    'We may share your personal information with:';

const String SHARING_PERSONAL_INFORMATION_1 =
    'Grupo Users . Any information you post, including comments, replies and other information, becomes publicly available. We cannot control how your information is used by others. We do not share your name or contact information with others. However, we do share your chosen user name. Also, your posts are available to all other users, and you may reveal your identity to others based on the content of your posts. Users can also freely share any Grupo content no matter who posted it, through other third party websites such as Facebook, Twitter, Instagram, and other social media services.';

const String SHARING_PERSONAL_INFORMATION_2 =
    'Service Providers . Third party vendors, consultants and other service providers that perform services on our behalf or in order to carry out their work for us, which may include payment processing, content or service fulfillment, or providing analytics services;';

const String SHARING_PERSONAL_INFORMATION_3 =
    'Business Transfers . We may transfer the information we have collected to another company in connection with, or during negotiations of, any merger, sale of company assets, financing, or acquisition of all or a portion of our business.';

const String SHARING_PERSONAL_INFORMATION_4 =
    'Third Parties and Partners . We may share information with our third party partners, including advertisers and researchers. For example, if you post content that mentions one of our advertisers, we may share that content with them. Or, we may work with a researcher to analyze the content of a particular feed.';

const String SHARING_PERSONAL_INFORMATION_5 =
    'Compliance with Legal Proceedings . We also may disclose the information in order to comply with the law, a judicial proceeding, court order, subpoena, or other legal process. For example, we may share your IP address or other identifier, your location information, or any other information we have collected about you and your device.';

const String SHARING_PERSONAL_INFORMATION_6 =
    'To Protect Us and Others. We may disclose information if we believe it is necessary to investigate, prevent, or take action regarding illegal activities, suspected fraud, situations involving potential threats to the safety of any person, violations of our Terms of Use or this Policy, or as evidence in litigation in which Grupo is involved.';

const String SHARING_PERSONAL_INFORMATION_7 =
    'Aggregate and De-Identified Information . We may share aggregate or de-identified information about users with third parties for marketing, advertising, research or similar purposes. If we associate aggregate or de-identified information with other information that when combined identifies an individual, we will treat the combined data as personal information.';

// YOUR CHOICES REGARDING YOUR INFORMATION
const String CHOISE_READING_INFORMATION_1 =
    'Account Data . You can edit and change your profile and account data through “Settings” to control whether your profile can be seen by other Users or by the general public.';

const String CHOISE_READING_INFORMATION_2 =
    'Data Retention . You may delete content that you have posted to Grupo at anytime. When you delete content from your account, the content will be hidden from other users. However, we will maintain a copy of the deleted content in our records for up to one year, unless legally required to maintain the record for a longer period. We will retain your information for as long as your account is active or as needed to provide you the Service. If your account becomes inactive, we will maintain your personal information in the event you wish to re-engage in our Service.';

// SECURITY AND STORAGE OF INFORMATION
const String SECURITY_1 =
    'Grupoza cares about the security of your information and uses commercially reasonable physical, administrative, and technological safeguards to preserve the integrity and security of all information we collect and that we share with our service providers. However, no security system is impenetrable and we cannot guarantee the security of our systems 100%. In the event that any information under our control is compromised as a result of a breach of security, we will take reasonable steps to investigate the situation and where appropriate, notify those individuals whose information may have been compromised and take other steps, in accordance with any applicable laws and regulations.';

const String SECURITY_2 =
    'Your information collected through the Service may be stored and processed in the United States. If you are located in the European Union or other regions with laws governing data collection and use that may differ from U.S. law, please note that we may transfer information, including personal information, to a country and jurisdiction that does not have the same data protection laws as your jurisdiction, and you consent to the transfer of information to the U.S. or any other country in which Grupo or its affiliates or service providers maintain facilities and the use and disclosure of information about you described in this Privacy Policy.';

// LINKS TO THIRD-PARTY WEBSITES
const String LINKS_TO_THIRD_PARTY =
    'The Service may contain links to and from third party websites of our business partners, advertisers, and social media sites. If you follow a link to any of these websites, please note that these websites have their own privacy policies and that we do not accept any responsibility or liability for their policies. Please check the individual policies before you submit any information to those websites.';

// OUR POLICY TOWARDS CHILDREN
const String OUR_POLICY_TOWARDS_CHILDREN =
    'We do not knowingly collect personal information from children under 13. If we become aware that a child under 13 has provided us with personal information, we take steps to remove such information and terminate the child\'s account. If you become aware that your child has provided us with personal information without your consent, please contact us at support@grupoza.com.';

// CALIFORNIA DO-NOT-TRACK DISCLOSURE
const String CALIFORNIA_DO_NOT_TRACK_DISCLOSURE =
    'Because we want to provide you with a personalized experience, both in terms of functionality and advertising, we do not change our behavior based on a web browser’s Do-Not-Track signal.';

// UPDATES TO THIS PRIVACY POLICY
const String UPDATES_TO_THIS_PRIVACY_POLICY =
    'We reserve the right to modify this Privacy Policy from time to time and so you should review this page periodically. If we make a change to this policy that, in our sole discretion, is material, we will notify you via a notification via the Service or email to the email address associated with your Grupo account, if any. By continuing to access or use the Service after those changes become effective, you agree to be bound by the revised Privacy Policy. When we change our Privacy Policy in a material manner, we will update the “last modified” date at the bottom of this page and attempt to notify you via email to the email address associate with your account.';

// MODIFIED
const String MODIFIED_PRIVACY =
    'This Privacy Policy was last modified on November 21, 2015';

const String CONTACT_PRIVACY =
    'Thoughts or questions about this Privacy Policy? Please, let us know .';
