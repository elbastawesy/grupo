// Terms of Use

// title of terms screen
const String TERMS_TITLE = 'TERMS OF USE AGREEMENT';

// introduction
const String TERMS_INTRODUCTION =
    'This Terms of Use Agreement (“Agreement”) explains the terms by which you may use the website (available at http://www.grupoza.com/ ), online service, and the Grupo mobile application (“Grupo”) (collectively the “Service”) made available by Grupoza Inc. (“Grupoza,” “we,” or “us”). By accessing or using the Service, you signify that you have read, understood, and agree to be bound by this Agreement and to the collection and use of your information as set forth in our Privacy Policy . Grupoza reserves the right to make unilateral modifications to these terms and will provide notice of these changes as described below. This Agreement applies to all visitors, users, and others who access the Service (“Users”).';

// second introductin
const String TERMS_SECOND_INTRODUCTION =
    'PLEASE READ THIS AGREEMENT CAREFULLY TO ENSURE THAT YOU UNDERSTAND EACH PROVISION. THIS AGREEMENT CONTAINS A MANDATORY INDIVIDUAL ARBITRATION AND CLASS ACTION/JURY WAIVER PROVISION THAT REQUIRES THE USE OF ARBITRATION ON AN INDIVIDUAL BASIS TO RESOLVE DISPUTES, RATHER THAN JURY TRIALS OR CLASS ACTIONS.';

// TERMS SERVICES
const String TERMS_SERVICE =
    'Grupoza, through the Service, provides a place for people to connect in an entirely new and unique way based on location. The Service is about making the digital world real by enabling people to directly communicate with others around them.';

const String SERVICE_1 =
    'Crowd Social Networking : With our social media platform Users can get in touch with others Grupo users (each, a “User”) who share similar interests in a friendly and interactive way that allows Users to filter a search, find more realistic results and validate the results.';

const String SERVICE_2 =
    'Crowd Mobile Domain : With our crowd mobile interaction app, we allow you to interact with Users around you based on your location. They become a part of your community and when  you share something, they see it. When they share something, you see it.';

const String SERVICE_2_A =
    'Eligibility . You must read and agree to these terms before using the Service. If you do not agree, you may not use the Service. Any use or access to the Service by anyone under 13 is strictly prohibited and in violation of this Agreement. The Service is not available to any Users previously removed from the Service by Grupo.';
const String SERVICE_2_B =
    'License . Subject to the terms and conditions of this Agreement, you are hereby granted a non-exclusive, limited, non-transferrable, freely revocable, license to use this Service for your personal, noncommercial use only and as permitted by the features of the Service. Grupoza reserves all rights not expressly granted herein in the Service and Grupoza Content (as defined below). Grupoza may terminate this license at any time for any reason.';

const String SERVICE_2_C =
    'Service Rules . You agree not to engage in any of the following prohibited activities: (i) copying, distributing, or disclosing any part of the Service in any medium, including without limitation by any automated or non-automated “scraping;” (ii) attempting to interfere with, compromise the system integrity or security or decipher any transmission to or from the servers running the Service; (iii) uploading invalid-data, viruses, worms, or other software agents through the Service; (iv) using the Service for any commercial solicitation purposes; (v) impersonating another person or otherwise misrepresenting your affiliation with a person or entity, conducting fraud, hiding or attempting to hide your identity; (vi) interfering with the proper working of the Service; (vii) accessing any content on the Service through any technology or means other than those provided or authorized by the Service; or (viii) bypassing the measures we may use to prevent or restrict access to the Service.';

const String SERVICE_END =
    'We may, without prior notice, change the Service; stop providing the Service or features of the Service, to you or to Users generally, or create usage limits for the Service. We may permanently or temporarily terminate or suspend your access to the Service without notice and liability for any reason, including if in our sole determination you violate any provision of this Agreement, or for no reason. Upon termination for any reason or for no reason, you may continue to be bound by this Agreement.';

// USER CONTENT
const String USER_CONTENT_1 =
    'Some areas of the Service allow Users to post or provide content such as text, audio, location, videos, photos, links, and other content or information (“User Content”). We claim no ownership rights over User Content created by you. The User Content you create remains yours, however, by providing or sharing User Content through the Service, you agree to allow others to view and/or share your User Content in accordance with your settings and this Agreement. We have the right (but not the obligation) in our sole discretion to remove any User Content that is shared via the Service.';

const String USER_CONTENT_2 =
    'You agree not to post User Content that: (i) may create a risk of harm, loss, physical or mental injury, emotional distress, death, disability, disfigurement, or physical or mental illness to you or to any other person; (ii) may create a risk of any other loss or damage to any person or property; (iii) may constitute or contribute to a crime or tort; (iv) contains any information or content that we deem to be unlawful, harmful, abusive, racially or ethnically offensive, defamatory, infringing, invasive to personal privacy or publicity rights, harassing, humiliating to other people (publicly or otherwise), libelous, threatening, profane, obscene, or otherwise objectionable; (v) contains any information or content that is illegal (including, without limitation, the disclosure of insider information under securities law or another party’s trade secrets); or (vi) contains any information or content that you do not have a right to make available under any law or under contractual or fiduciary relationships.';

const String USER_CONTENT_3 =
    'For the purposes of this Agreement, “Intellectual Property Rights” means all patent rights, copyright rights, trademark, trade dress and service mark rights, goodwill, trade secret rights and other intellectual property rights as may now exist or hereafter come into existence, and all applications therefore and registrations, renewals and extensions thereof, under the laws of any state, country, territory or other jurisdiction.';

const String USER_CONTENT_4 =
    'We take no responsibility and assume no liability for any User Content that you or any other User or third party posts, sends, or otherwise makes available over the Service. You shall be solely responsible for your User Content and the consequences of posting, publishing it, sharing it, or otherwise making it available on the Service, and you agree that we are only acting as a passive conduit for your online distribution and publication of your User Content. You understand and agree that you may be exposed to User Content that is inaccurate, objectionable, or otherwise inappropriate, and you agree Grupoza shall not be liable for any damages you allege to incur as a result of or relating to any User Content.';

// USER CONTENT LICENSE GRANT
const String USER_CONTENT_LICENSE_GRANT =
    'By posting or otherwise making available any User Content on or through the Service, you expressly grant, and you represent and warrant that you have the rights necessary to grant to Grupoza, a royalty-free, sublicensable, transferable, perpetual, irrevocable, non-exclusive, worldwide license to use, reproduce, modify, publish, edit, translate, distribute, publicly perform, publicly display, and make derivative works of, all such User Content, in whole or in part, and in any form, media or technology, whether now known or hereafter developed, for use in connection with the Service and Grupoza’s (and its successors’ and affiliates’) business, including without limitation for promoting and redistributing part or all of the Service (and derivative works thereof) in any media formats and through any media channels.';

// GRUPO MOBILE APP
const String GURPO_MOBILE_APP_A_1 =
    'The Software . With our crowd mobile interaction app, we allow you to interact with Users around you based on your location (referred to herein as “Mobile Software”). To use the Mobile Software you must have a mobile device that is compatible with the Mobile Software. Grupoza does not warrant that the Mobile Software will be compatible with your mobile device. You may use mobile data in connection with the Mobile Software and may incur additional charges from your wireless provider for these services. You agree that you are solely responsible for any such charges. Grupoza hereby grants you a non-exclusive, non-transferable, revocable license to use a compiled code copy of the Mobile Software for one Grupo account on one mobile device owned or leased solely by you, for your personal use.';

const String GURPO_MOBILE_APP_A_2 =
    'You may not: (i) modify, decompile or reverse engineer the Mobile Software to the extent that such a restriction is expressly prohibited by law; (ii) rent, lease, loan, resell, sublicense, distribute or otherwise transfer the Mobile Software to any third party or use the Mobile Software to provide time sharing or similar services to any third party; (iii) make any copies of the Mobile Software; (iv) remove, circumvent, disable, damage or otherwise interfere with security-related features of the Mobile Software, features that prevent or restrict use or copying of any content accessible through the Mobile Software, or features that enforce limitations on use of the Mobile Software; or (v) delete the copyright and other proprietary rights notices on the Mobile Software. You acknowledge that Grupoza may from time to time issue upgraded versions of the Mobile Software, and may automatically electronically upgrade the version of the Mobile Software that you are using on your mobile device. You consent to such automatic upgrading on your mobile device, and agree that the terms and conditions of this Agreement will apply to all such upgrades.';

const String GURPO_MOBILE_APP_B =
    'Mobile Application Marketplaces . The following applies to Mobile Software you acquire from the iTunes Store, the Android Market, or any other similar marketplace for mobile applications (“Sourced Mobile Software”): You acknowledge and agree that this Agreement is solely between you and Grupoza, not the proprietor of such marketplace (each a “Proprietor”), and that Proprietor has no responsibility for the Sourced Mobile Software or content thereof. Your use of the Sourced Mobile Software must comply with the applicable terms of service for the marketplace from which you downloaded the Sourced Mobile Software. You acknowledge that Proprietor has no obligation whatsoever to furnish any maintenance and support services with respect to the Sourced Mobile Software. In the event of any failure of the Sourced Mobile Software to conform to any applicable warranty, you may notify Proprietor, and Proprietor will refund the purchase price for the Sourced Mobile Software to you; to the maximum extent permitted by applicable law, Proprietor will have no other warranty obligation whatsoever with respect to Sourced Mobile Software, and any other claims, losses, liabilities, damages, costs or expenses attributable to any failure to conform to any warranty will be solely governed by this Agreement and any law applicable to Grupoza as provider of the software. You acknowledge that Proprietor is not responsible for addressing any claims of you or any third party relating to the Sourced Mobile Software or your possession and/or use of the Sourced Mobile Software, including, but not limited to: (i) product liability claims; (ii) any claims that the Sourced Mobile Software fails to conform to any applicable legal or regulatory requirement; and (iii) claims arising under consumer protection or similar legislation; and all such claims are governed by this Agreement and any law applicable to Grupoza as provider of the software. You acknowledge that, in the event any third-party claim that the Sourced Mobile Software infringes that third party’s intellectual property rights, Grupo, not Proprietor, will be solely responsible for the investigation, defense, settlement and discharge of any such intellectual property infringement claim to the extent required under this Agreement. You and Grupoza acknowledge and agree that Proprietor, and Proprietor’s subsidiaries, are third-party beneficiaries of this Agreement as relates to your license of the Sourced Mobile Software, and that, upon your acceptance of the terms and conditions of this Agreement, Proprietor will have the right (and will be deemed to have accepted the right) to enforce this Agreement as relates to your license of the Sourced Mobile Software against you as a third-party beneficiary thereof.';

// OUR PROPRIETARY RIGHTS.
const String OUR_PROPRIETARY_RIGHTS =
    'OUR PROPRIETARY RIGHTS. Except for your User Content, the Service and all materials therein or transferred thereby, including, without limitation, software, text, images, graphics, illustrations, logos, patents, trademarks, service marks, copyrights, photographs, video, audio, and User Content belonging to other Users (“Grupoza Content”), and all Intellectual Property Rights related thereto, are the exclusive property of Grupoza and its licensors (including other Users who post User Content to the Service). Except as explicitly provided herein, nothing in this Agreement shall be deemed to create alicense in or under any such Intellectual Property Rights, and you agree not to sell, license, rent, modify, distribute, copy, reproduce, transmit, publicly display, publicly perform, publish, adapt, edit or create  derivative works from any Grupoza Content. Use of Grupoza Content for any purpose not expressly permitted by this Agreement is strictly prohibited.';

// PRIVACY
const String PRIVACY =
    'PRIVACY. We care about the privacy of our Users. You understand that by using the Services you consent to the collection, use, and disclosure of your personally identifiable information and aggregate data as set forth in our Privacy Policy, and to have your personally identifiable information collected, used, transferred to, and processed in the United States.';

// SECURITY
const String SECURITY =
    'SECURITY. Grupoza cares about the integrity and security of your personal information. However, we cannot guarantee that unauthorized third parties will never be able to defeat our security measures or use your personal information for improper purposes. You acknowledge that you provide your personal information at your own risk.';

// DMCA NOTICE
const String DMCA_NOTICE =
    'DMCA NOTICE. It is Grupoza\'s policy to respond to alleged infringement notices that comply with the Digital Millennium Copyright Act of 1998 (“DMCA”).';

const String DMCA_NOTICE_1 =
    "If you believe that your copyrighted work has been copied in a way that constitutes copyright infringement and accessible via the Service, please notify Grupoza's copyright agent as set forth in the DMCA. For your complaint to be valid under the DMCA, you must provide the following information in writing:";

const String DMCA_NOTICE_1_1 =
    'An electronic or physical signature of a person authorized to act on behalf of the copyright owner;';

const String DMCA_NOTICE_1_2 =
    'Identification of the copyrighted work that you claim has been infringed;';

const String DMCA_NOTICE_1_3 =
    'Identification of the material that is claimed to be infringing and where it is located on the Service;';

const String DMCA_NOTICE_1_4 =
    'Information reasonably sufficient to permit Grupoza to contact you, such as your address, telephone number, and e-mail address;';

const String DMCA_NOTICE_1_5 =
    'A statement that you have a good faith belief that use of the material in the manner complained of is not authorized by the copyright owner, its agent, or law;';

const String DMCA_NOTICE_1_6 =
    'A statement made under penalty of perjury, that the above information is accurate, and that you are the copyright owner or are authorized to act on behalf of the owner.';

const String DMCA_NOTICE_2 =
    'UNDER FEDERAL LAW, IF YOU KNOWINGLY MISREPRESENT THAT ONLINE MATERIAL IS INFRINGING, YOU MAY BE SUBJECT TO CRIMINAL PROSECUTION FOR PERJURY AND CIVIL PENALTIES, INCLUDING MONETARY DAMAGES, COURT COSTS, AND ATTORNEYS’ FEES.';

const String DMCA_NOTICE_3 =
    'Please note that this procedure is exclusively for notifying Grupoza and its affiliates that your copyrighted material has been infringed. The preceding requirements are intended to comply with Grupoza\'s rights and obligations under the DMCA including 17 U.S.C. § 512(c), but do not constitute legal advice. It may be advisable to contact an attorney regarding your rights and obligations under the DMCA and other applicable laws.';

const String DMCA_NOTICE_4 =
    'In accordance with the DMCA and other applicable law, Grupoza has adopted a policy of terminating, in appropriate circumstances, Users who are deemed to be repeat infringers. Grupoza may also at its sole discretion limit access to the Service and/or terminate the accounts of any Users who infringe any intellectual property rights of others, whether or not there is any repeat infringement.';

// THIRD-PARTY LINKS AND INFORMATION.

const String THIRD_PARTY_LINKS_AND_INFORMATION =
    'THIRD-PARTY LINKS AND INFORMATION. The Service may contain links to third-party materials that are not owned or controlled by Grupo. Grupoza does not endorse or assume any responsibility for any such third-party sites, information, materials, products, or services. If you access a third-party website or service from the Service or share your User Content on or through any third-party website or service, you do so at your own risk, and you understand that this Agreement and Grupoza’s Privacy Policy do not apply to your use of such sites. You expressly relieve Grupoza from any and all liability arising from you use of any third-party website, service, or content, including without limitation User Content submitted by other Users. Additionally, your dealings with or participation in promotions of advertisers found on the Service, including payment and delivery of goods, and any other terms (such as warranties) are solely between you and such advertisers. You agree that Grupoza shall not be responsible for any loss or damage of any sort relating to your dealings with such advertisers.';

// INDEMNITY
const String INDEMNITY =
    'INDEMNITY. You agree to defend, indemnify and hold harmless Grupoza and its subsidiaries, agents, licensors, managers, and other affiliated companies, and their employees, contractors, agents, officers and directors, from and against any and all claims, damages, obligations, losses, liabilities, costs or debt, and expenses (including but not limited to attorney’s fees) arising from: (i) your use of and access to the Service, including any data or content transmitted or received by you; (ii) your violation of any term of this Agreement, including without limitation your breach of any of the representations and warranties above; (iii) your violation of any third-party right, including without limitation any right of privacy or Intellectual Property Rights; (iv) your violation of any applicable law, rule or regulation; (v) User Content or any content that is submitted via your account including without limitation misleading, false or inaccurate information; (vi) your willful misconduct; or (vii) any other party’s access and use of the Service with your unique username, password or other appropriate security code.';

// NO WARRANTY.
const String NO_WARRANTY =
    'NO WARRANTY. THE SERVICE IS PROVIDED ON AN “AS IS” AND “AS AVAILABLE” BASIS. USE OF THE SERVICE IS AT YOUR OWN RISK. TO THE MAXIMUM EXTENT PERMITTED BY APPLICABLE LAW, THE SERVICE IS PROVIDED WITHOUT WARRANTIES OF ANY KIND, WHETHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO, IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT. NO ADVICE OR INFORMATION, WHETHER ORAL OR WRITTEN, OBTAINED BY YOU FROM GRUPOZA OR THROUGH THE SERVICE WILL CREATE ANY WARRANTY NOT EXPRESSLY STATED HEREIN. WITHOUT LIMITING THE FOREGOING, GRUPO, ITS AFFILIATES, AND ITS LICENSORS DO NOT WARRANT THAT THE GRUPOZA CONTENT IS ACCURATE, RELIABLE, OR CORRECT; THAT THE SERVICE WILL MEET YOUR REQUIREMENTS; THAT THE SERVICE WILL BE AVAILABLE AT ANY PARTICULAR TIME OR LOCATION, UNINTERRUPTED OR SECURE; THAT ANY DEFECTS OR ERRORS WILL BE CORRECTED; OR THAT THE SERVICE IS FREE OF VIRUSES OR OTHER HARMFUL COMPONENTS. ANY CONTENT DOWNLOADED OR OTHERWISE OBTAINED THROUGH THE USE OF THE SERVICE IS DOWNLOADED AT YOUR OWN RISK AND YOU WILL BE SOLELY RESPONSIBLE FOR ANY DAMAGE TO YOUR COMPUTER SYSTEM OR MOBILE DEVICE OR LOSS OF DATA THAT RESULTS FROM SUCH DOWNLOAD OR YOUR USE OF THE SERVICE.';

const String NO_WARRRANTY_2 =
    'GRUPOZA DOES NOT WARRANT, ENDORSE, GUARANTEE, OR ASSUME RESPONSIBILITY FOR ANY PRODUCT OR SERVICE ADVERTISED OR OFFERED BY A THIRD PARTY THROUGH THE SERVICE OR ANY HYPERLINKED WEBSITE OR SERVICE, AND GRUPOZA WILL NOT BE A PARTY TO OR IN ANY WAY MONITOR ANY TRANSACTION BETWEEN YOU AND A THIRD-PARTY PROVIDERS OF PRODUCTS OR SERVICES.';

const String NO_WARRRANTY_3 =
    'FEDERAL LAW, SOME STATES, PROVINCES AND OTHER JURISDICTIONS DO NOT ALLOW THE EXCLUSION AND LIMITATIONS OF CERTAIN IMPLIED WARRANTIES, SO THE ABOVE EXCLUSIONS MAY NOT APPLY TO YOU. THIS AGREEMENT GIVES YOU SPECIFIC LEGAL RIGHTS, AND YOU MAY ALSO HAVE OTHER RIGHTS WHICH VARY FROM STATE TO STATE. THE DISCLAIMERS AND EXCLUSIONS UNDER THIS AGREEMENT WILL NOT APPLY TO THE EXTENT PROHIBITED BY APPLICABLE LAW.';

// LIMITATION OF LIABILITY.
const String LIMITATION_OF_LIABILITY_1 =
    'LIMITATION OF LIABILITY. TO THE MAXIMUM EXTENT PERMITTED BY APPLICABLE LAW, IN NO EVENT SHALL GRUPOZA, ITS AFFILIATES, AGENTS, DIRECTORS, EMPLOYEES, SUPPLIERS OR LICENSORS BE LIABLE FOR ANY INDIRECT, PUNITIVE, INCIDENTAL, SPECIAL, CONSEQUENTIAL OR EXEMPLARY DAMAGES, INCLUDING WITHOUT LIMITATION DAMAGES FOR LOSS OF PROFITS, GOODWILL, USE, DATA OR OTHER INTANGIBLE LOSSES, ARISING OUT OF OR RELATING TO THE USE OF, INABILITY TO USE, THIS SERVICE. UNDER NO CIRCUMSTANCES WILL GRUPOZA BE RESPONSIBLE FOR ANY DAMAGE, LOSS OR INJURY RESULTING FROM HACKING, TAMPERING OR OTHER UNAUTHORIZED ACCESS OR USE OF THE SERVICE OR YOUR ACCOUNT OR THE INFORMATION CONTAINED THEREIN.';

const String LIMITATION_OF_LIABILITY_2 =
    'TO THE MAXIMUM EXTENT PERMITTED BY APPLICABLE LAW, GRUPOZA ASSUMES NO LIABILITY OR RESPONSIBILITY FOR ANY (I) ERRORS, MISTAKES, OR INACCURACIES OF CONTENT; (II) PERSONAL INJURY OR PROPERTY DAMAGE, OF ANY NATURE WHATSOEVER, RESULTING FROM YOUR ACCESS TO OR USE OF OUR SERVICE; (III) ANY UNAUTHORIZED ACCESS TO OR USE OF OUR SECURE SERVERS AND/OR ANY AND ALL PERSONAL INFORMATION STORED THEREIN; (IV) ANY INTERRUPTION OR CESSATION OF TRANSMISSION TO OR FROM THE SERVICE; (V) ANY VIRUSES, TROJAN HORSES, OR THE LIKE THAT MAY BE TRANSMITTED TO OR THROUGH OUR SERVICE BY ANY THIRD PARTY; (VI) ANY ERRORS OR OMISSIONS IN ANY CONTENT OR FOR ANY LOSS OR DAMAGE INCURRED AS A RESULT OF THE USE OF ANY CONTENT POSTED, EMAILED, TRANSMITTED, OR OTHERWISE MADE AVAILABLE THROUGH THE SERVICE; AND/OR (VII) USER CONTENT OR THE DEFAMATORY, OFFENSIVE, OR ILLEGAL CONDUCT OF ANY THIRD PARTY. IN NO EVENT SHALL GRUPO, ITS AFFILIATES, AGENTS, DIRECTORS, EMPLOYEES, OR LICENSORS BE LIABLE TO YOU FOR ANY CLAIMS, PROCEEDINGS, LIABILITIES, OBLIGATIONS, DAMAGES, LOSSES OR COSTS IN AN AMOUNT EXCEEDING THE AMOUNT YOU PAID TO GRUPOZA HEREUNDER.';

const String LIMITATION_OF_LIABILITY_3 =
    'THIS LIMITATION OF LIABILITY SECTION APPLIES WHETHER THE ALLEGED LIABILITY IS BASED ON CONTRACT, TORT, NEGLIGENCE, STRICT LIABILITY, OR ANY OTHER BASIS, EVEN IF GRUPOZA HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. THE FOREGOING LIMITATION OF LIABILITY SHALL APPLY TO THE FULLEST EXTENT PERMITTED BY LAW IN THE APPLICABLE JURISDICTION. SOME STATES DO NOT ALLOW THE EXCLUSION OR LIMITATION OF INCIDENTAL OR CONSEQUENTIAL DAMAGES, SO THE ABOVE LIMITATIONS OR EXCLUSIONS MAY NOT APPLY TO YOU. THIS AGREEMENT GIVES YOU SPECIFIC LEGAL RIGHTS, AND YOU MAY ALSO HAVE OTHER LEGAL RIGHTS WHICH VARY FROM STATE TO STATE. THE DISCLAIMERS, EXCLUSIONS, AND LIMITATIONS OF LIABILITY UNDER THIS AGREEMENT WILL NOT APPLY TO THE EXTENT PROHIBITED BY APPLICABLE LAW.';

const String LIMITATION_OF_LIABILITY_4 =
    'The Service is controlled and operated from facilities in the United States. Grupoza makes no representations that the Service is appropriate or available for use in other locations. Those who access or use the Service from other jurisdictions do so at their own volition and are entirely responsible for compliance with all applicable United States and local laws and regulations, including but not limited to export and import regulations. You may not use the Service if you are a resident embargoed by the United States or a foreign person or entity blocked or denied by the United States government. Unless otherwise explicitly stated, all materials found on the Service are solely directed to individuals, companies, or other entities located in the United States.';

// GOVERNING LAW, ARBITRATION, AND CLASS ACTION/JURY TRIAL WAIVER
const String GOVERNING_LAW_A =
    'Governing Law . You agree that: (i) the Service shall be deemed solely based in Delaware; and (ii) the Service shall be deemed a passive one that does not rise to personal jurisdiction over us, either specific or general, in jurisdictions other than Delaware. This Agreement shall be governed by the internal substantive laws of the State of Delaware, without respect to its conflict of laws principles. The parties acknowledge that this Agreement evidences a transaction involving interstate commerce. Notwithstanding the preceding sentences with respect to the substantive law, any arbitration conducted pursuant to the terms of this Agreement shall be governed by the Federal Arbitration Act (9 U.S.C. §§ 1-16). The application of the United Nations Convention on Contracts for the International Sale of Goods is expressly excluded. You agree to submit to the personal jurisdiction of the federal and state courts located in New Castle County, Delaware for any actions for which we retain the right to seek injunctive relief or other equitable relief in a court of competent jurisdiction to prevent the actual or threatened infringement, misappropriation or violation of our copyrights, trademarks, trade secrets, patents, or other intellectual property or proprietary rights, as set forth in the Arbitration provision below, including any provisional relief required to prevent irreparable harm. You agree that New Castle County, Delaware is the proper forum for any appeals of an arbitration award or for trial court proceedings in the event that the arbitration provision below is found to be unenforceable.';

const String GOVERNING_LAW_B =
    'Arbitration. READ THIS SECTION CAREFULLY BECAUSE IT REQUIRES THE PARTIES TO ARBITRATE THEIR DISPUTES AND LIMITS THE MANNER IN WHICH YOU CAN SEEK RELIEF FROM GRUPO. For any dispute with Grupo, you agree to first contact us and attempt to resolve the dispute with us informally. In the unlikely event that Grupoza has not been able to resolve the dispute it has with you after sixty (60) days, we each agree to resolve any claim, dispute, or controversy (excluding any claims for injunctive or other equitable relief as provided below) arising out of or in connection with or relating to this Agreement, or the breach or alleged breach thereof (collectively, “Claims”), by binding arbitration by JAMS, under the Optional Expedited Arbitration Procedures then in effect for JAMS, except as provided herein. JAMS may be contacted at www.jamsadr.com . The arbitration will be conducted New Castle County, Delaware unless you wish Grupoza to agree otherwise. If you are using the Service for commercial purposes, each party will be responsible for paying any JAMS filing, administrative and arbitrator fees in accordance with JAMS rules, and the award rendered by the arbitrator shall exclude costs of arbitration, reasonable attorneys’ fees and reasonable cost for expert and other witnesses. If you are an individual using the Service for non-commercial purposes: (i) JAMS may require you to pay a fee for the initiation of your case, unless you apply for and successfully obtain a fee waiver from JAMS; (ii) the award rendered by the arbitrator may include your costs of arbitration, your reasonable attorney fees, and your reasonable costs for expert and other witnesses; and (iii) you may sue in a small claims court of competent jurisdiction without first engaging in arbitration, but this does not absolve you or your commitment to engage in the informal dispute resolution process. Any judgment on the award rendered by the arbitrator may be entered in a court of competent jurisdiction. Nothing in this Section shall be deemed as preventing Grupoza from seeking injunctive or other equitable relief from the courts as necessary to prevent actual or threatened infringement, misappropriation, or violation of our data security, Intellectual Property Rights or other proprietary rights.';

const String GOVERNING_LAW_C =
    'Class Action/Jury Trial Waiver . WITH RESPECT TO ALL PERSONS AND ENTITIES, REGARDLESS OF WHETHER THEY HAVE OBTAINED OR USED THE SERVICE OR PERSONAL, COMMERCIAL OR OTHER PURPOSES, ALL CLAIMS MUST BE BROUGHT IN THE PARTIES’ INDIVIDUAL CAPACITY, AND NOT AS A PLAINTIFF OR CLASS MEMBER IN ANY PURPORTED CLASS ACTION, COLLECTIVE ACTION, PRIVATE ATTORNEY GENERAL ACTION OR OTHER REPRESENTATIVE PROCEEDING. THIS WAIVER APPLIES TO CLASS ARBITRATION, AND, UNLESS WE AGREE OTHERWISE, THE ARBITRATOR MAY NOT CONSOLIDATE MORE THAN ONE PERSON’S CLAIMS. YOU AGREE THAT, BY ENTERING INTO THIS AGREEMENT, YOU AND GRUPOZA ARE EACH WAIVING THE RIGHT TO A TRIAL BY JURY OR TO PARTICIPATE IN A CLASS ACTION, COLLECTIVE ACTION, PRIVATE ATTORNEY GENERAL ACTION, OR OTHER REPRESENTATIVE PROCEEDING OF ANY KIND.';

// GENERAL
const String GENERAL_A =
    'Assignment . This Agreement, and any rights and licenses granted hereunder, may not be transferred or assigned by you, but may be assigned by Grupoza without restriction. Any attempted transfer or assignment in violation hereof shall be null and void.';

const String GENERAL_B =
    'Notification Procedures and Changes to the Agreement . Grupoza may provide notifications, whether such notifications are required by law or are for marketing purposes or other business related purposes, to you through the use of our Service, written or hard-copy notice, or through posting of such notice on our website, as determined by Grupoza in our sole discretion. Grupoza reserves the right to determine the form and means of providing notifications to our Users, provided that you may opt out of certain means of notification as described in this Agreement. Grupoza may, in its sole discretion, modify or update this Agreement from time to time, and so you should review this page periodically. When we change the Agreement in a material manner, we will update the “last modified” date at the bottom of this page. Your continued use of the Service after any such change constitutes your acceptance of the new Terms of Use. If you do not agree to any of these terms or any future Terms of Use, do not use or access (or continue to access) the Service.';
const String GENERAL_C =
    'Entire Agreement/Severability . This Agreement, together with any amendments and any additional agreements you may enter into with Grupoza in connection with the Service, shall constitute the entire Agreement between you and Grupoza concerning the Service. If any provision of this Agreement is deemed invalid by a court of competent jurisdiction, the invalidity of such provision shall not affect the validity of the remaining provisions of this Agreement, which shall remain in full force and effect, except that in the event of unenforceability of the universal Class Action/Jury Trial Waiver, the entire arbitration agreement shall be unenforceable.';

const String GENERAL_D =
    'No Waiver. No waiver of any term of this Agreement shall be deemed a further or continuing waiver of such term or any other term, and Grupoza’s failure to assert any right or provision under this Agreement shall not constitute a waiver of such right or provision.';

const String GENERAL_E =
    'Please contact us at info@grupoza.com with any questions under this Agreement.';

// MODIFIED
const String MODIFIED = 'This Agreement was last modified on November 22, 2015';
