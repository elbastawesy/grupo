// regiter screen error messages
const String FIELD_IS_EMPTY_ERROR_MESSAGE = 'You must enter your phone number';
const String CORRECT_NUMBER_ERROR_MESSAGE = 'Enter the correct number';

// OTP screen error messages
const String INVALID_OTP_ERROR_MESSAGE = 'invalid OTP';

// add profile screen error messages
const String NICKNAME_ERROR_MESSAGE = 'you must enter your nickname.';
const String NICKNAME_LENGTH_ERROR_MESSAGE =
    'your name must be shorter than 10 charachters';

// choose avatar screen error messages
const String AVATAR_NAMe_ERROR_MESSAGE = 'Please choose your avatar.';

//choose range screen error messages
const String CHOOSE_RANGE__ERROR_MESSAGE = 'please choose chat range.';
const String ENABLE_SERVICE_ERROR_MESSAGE =
    'please enable access to your location.';
const String LOCATION_PERMISSION_DENIED_ERROR = 'location permission denied.';

// profile screen error messages
const String PROFILE_ERROR_MESSAGE =
    'something get wrong!. check your internet connection';

// message of deactivating my account
const String DE_ACTIVATE_MY_ACCOUNT_CONFIRM_MESSAGE =
    'Are you sure you want to de-activate your account?';

// favorite message is empty in favortie screen
const FAVORITE_MESSAGES_IS_EMPTY = 'there is no favorite messages';

// exit grupo message in more screen
const String EXIT_GRUPO_MESSAGE = 'Are you sure you want to exit Grupo ?';
