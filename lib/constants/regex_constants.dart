RegExp numberRegex = new RegExp(r'(^[0-9]{9,12}$)');
RegExp codeRegex = new RegExp(r'(^(?:[+0]5)?[0-5]{6}$)');
RegExp preventLettersRegex = new RegExp("[a-zA-Z]");