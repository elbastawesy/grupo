import 'package:sqflite/sqflite.dart' as sql;
import 'package:path/path.dart' as path;

class DBHelper {
  static Future<sql.Database> database() async {
    final dbPath = await sql.getDatabasesPath();
    return sql.openDatabase(
      path.join(dbPath, 'grupo.db'),
      onCreate: (db, version) => _createDb(db),
      version: 1,
    );
  }

  static void _createDb(sql.Database db) {
    db.execute('CREATE TABLE chat '
        '(messageId TEXT PRIMARY KEY, '
        'userName TEXT, userId TEXT, '
        'userAvatar TEXT, '
        'userNumber TEXT, '
        'createdAt TEXT, '
        'message TEXT, '
        'emoji TEXT, '
        'location TEXT, '
        'latitude DOUBLE, '
        'longitude DOUBLE, '
        'isFavorite INTEGER)');

    db.execute('CREATE TABLE favoriteMessages '
        '(docId TEXT PRIMARY KEY, '
        'userName TEXT, '
        'userAvatar TEXT, '
        'saveTime TEXT, '
        'message TEXT)');
  }

  static Future<void> insertData(
      {String tableName, Map<String, Object> data}) async {
    final db = await DBHelper.database();
    await db.insert(
      tableName,
      data,
      conflictAlgorithm: sql.ConflictAlgorithm.replace,
    );
  }

  static Future<List<Map<String, dynamic>>> getData({String tableName}) async {
    final db = await DBHelper.database();
    return db.query(tableName);
  }

  static Future<void> deleteDB(String tableName) async {
    final db = await DBHelper.database();
    return db.delete(tableName);
  }
}
