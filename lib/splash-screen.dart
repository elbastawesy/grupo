import 'dart:async';

import 'package:flutter/material.dart';
import 'package:grupo/save_date_locally_by_sharedPreferences.dart';
import 'package:grupo/screens/main-screen.dart';
import 'package:grupo/screens/welcome-screen.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    super.initState();
    startTime();
  }

  startTime() async {
    var _duration = Duration(seconds: 4);
    return Timer(_duration, checkIfFirstUse);
  }

  checkIfFirstUse() async {
    var checkVisited = await SaveDataLocally().checkVisited();

    if (checkVisited) {
      Navigator.of(context).pushReplacementNamed(MainScreen.routeName);
    } else {
      Navigator.of(context).pushReplacementNamed(WelcomeScreen.routeName);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        height: double.infinity,
        width: double.infinity,
        child: Image(
          image: AssetImage('assets/images/launch_icon.png'),
          fit: BoxFit.cover,
        ),
      ),
    );
  }
}
