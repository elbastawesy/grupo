import 'dart:io';

import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';

import '../save_date_locally_by_sharedPreferences.dart';

class Notifications {
  String content;
  DateTime dateTime;

  Notifications({this.content, this.dateTime});
}

class NotificationProvider with ChangeNotifier {
  Future<void> _showNotification(
      localNotifications, String title, String body) async {
    var androidDetails = AndroidNotificationDetails(
      'channelId',
      'local notification',
      'this is push notification coming from firebase',
      importance: Importance.high,
    );
    var iosDetails = IOSNotificationDetails();
    var generalNotificationDetails = NotificationDetails(
      android: androidDetails,
      iOS: iosDetails,
    );
    await localNotifications.show(0, title, body, generalNotificationDetails);
  }

  void receivePushNotification(localNotifications) async {
    var getSavedData = SaveDataLocally();
    var noti = await getSavedData.allowNotifications();
    var allowNotifications = noti;

    if (allowNotifications) {
      final fbm = FirebaseMessaging();
      if (Platform.isIOS) {
        fbm.requestNotificationPermissions();
      }
      fbm.configure(
        onMessage: (msg) async {
          return _showNotification(
            localNotifications,
            msg['notification']['title'],
            msg['notification']['body'],
          );
        },
        onLaunch: (msg) {
          return;
        },
        onResume: (msg) {
          return;
        },
      );
    }
  }

  Future<void> clearNotificatons() async {
    try {
      final userId = FirebaseAuth.instance.currentUser.uid;
      await FirebaseDatabase.instance
          .reference()
          .child('notifications')
          .child(userId)
          .remove();
    } catch (e) {
      throw (e);
    }
  }
}
