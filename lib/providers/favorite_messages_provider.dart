import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:grupo/helper/db_helper.dart';
import '../model/favorite_messages_model.dart';

class FavoriteMessagesProvider with ChangeNotifier {
  List<FavoriteMessage> _favoriteMessageList = [];

  List<FavoriteMessage> get getMessagesList {
    return [..._favoriteMessageList];
  }

  void addFavoriteMessageToLocalDatabase(FavoriteMessage newMessage) async {
    final newFavoriteMessage = FavoriteMessage(
      message: newMessage.message,
      saveTime: newMessage.saveTime,
      userAvatar: newMessage.userAvatar,
      userName: newMessage.userName,
      id: newMessage.id,
    );
    _favoriteMessageList.add(newFavoriteMessage);
    notifyListeners();
    DBHelper.insertData(tableName: 'favoriteMessages', data: {
      'docId': newMessage.id,
      'userName': newMessage.userName,
      'userAvatar': newMessage.userAvatar,
      'saveTime': newMessage.saveTime.toString(),
      'message': newMessage.message,
    });
  }

  void makeMessageAsFavoriteAtFirebase(
      {String userId, String messageId, bool isFav}) {
    FirebaseDatabase.instance
        .reference()
        .child('messages')
        .child(userId)
        .child(messageId)
        .update({
      'isFavorite': isFav,
    });
  }

  Future<void> fetchFavoriteMessages() async {
    final dataList = await DBHelper.getData(
      tableName: 'favoriteMessages',
    );
    _favoriteMessageList = dataList.map((e) {
      return FavoriteMessage(
        message: e['message'],
        id: e['docId'],
        userAvatar: e['userAvatar'],
        userName: e['userName'],
        saveTime: DateTime.tryParse(e['saveTime']),
      );
    }).toList();
    notifyListeners();
  }
}
