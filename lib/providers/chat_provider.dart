import 'dart:async';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:grupo/model/chat_message_model.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;
import '../save_date_locally_by_sharedPreferences.dart';
import 'package:location/location.dart';
import 'package:geocoder/geocoder.dart';

class ChatProvider with ChangeNotifier {
  List<ChatMessage> _chatMessagesList = [];

  Stream<List<ChatMessage>> get getChat async* {
    _chatMessagesList.sort((a, b) => b.createdAt.compareTo(a.createdAt));
    yield _chatMessagesList.sublist(0);
  }

  // Future<void> addChatMessagesToLocalDB(
  //     {String message, String emoji, BuildContext context}) async {
  //   final userDataFromStorage = SaveDataLocally();
  //   final _userId = await userDataFromStorage.getUserId();
  //   final _userName = await userDataFromStorage.getUserName();
  //   final _userAvatar = await userDataFromStorage.getUserAvatar();
  //   final _userNumber = await userDataFromStorage.getUserPhoneNumber();

  //   var newChatMessage = ChatMessage(
  //     userAvatar: _userAvatar,
  //     userId: _userId,
  //     userName: _userName,
  //     userNumber: _userNumber,
  //     createdAt: DateTime.now(),
  //     emoji: emoji,
  //     isFavorite: false,
  //     latitude: 1,
  //     location: '',
  //     longitude: 1,
  //     message: message,
  //     messageId: '',
  //   );

  //   await DBHelper.insertData(tableName: 'chat', data: {
  //     'messageId': _userId,
  //     'userName': _userName,
  //     'userId': _userId,
  //     'userAvatar': _userAvatar,
  //     'userNumber': _userNumber,
  //     'createdAt': DateTime.now().toString(),
  //     'message': message,
  //     'emoji': emoji,
  //     'location': '',
  //     'latitude': 1,
  //     'longitude': 1,
  //     'isFavorite': 0,
  //   });
  //   _chatMessagesList.add(newChatMessage);
  //   _chatMessagesList.sort(
  //     (a, b) => b.createdAt.compareTo(a.createdAt),
  //   );
  //   notifyListeners();
  // }

  // Future<void> fetchChatMessagedFromLocalDB() async {
  //   print(' app is offline fetcing messaged from local db');
  //   try {
  //     final dataList = await DBHelper.getData(tableName: 'chat');

  //     _chatMessagesList = dataList.map((message) {
  //       bool fav;
  //       if (message['isFavorite'] == 1) {
  //         fav = true;
  //       } else {
  //         fav = false;
  //       }
  //       return ChatMessage(
  //         createdAt: DateTime.parse(message['createdAt']),
  //         emoji: message['emoji'],
  //         isFavorite: fav,
  //         latitude: message['latitude'],
  //         location: message['location'],
  //         longitude: message['longitude'],
  //         message: message['message'],
  //         messageId: message['messageId'],
  //         userAvatar: message['userAvatar'],
  //         userId: message['userId'],
  //         userName: message['userNumber'],
  //         userNumber: message['userNumber'],
  //       );
  //     }).toList();
  //     _chatMessagesList.sort((a, b) => b.createdAt.compareTo(a.createdAt));
  //     print(_chatMessagesList.toString());
  //     notifyListeners();
  //   } catch (e) {
  //     print('error fetching chat messages from local db');
  //     print(e);
  //   }
  // }

  // void fetchFromFirebaseAndAddingToLocalDB() {
  //   print('fetch from firebase to local db');
  //   final userId = FirebaseAuth.instance.currentUser.uid;
  //   FirebaseDatabase.instance
  //       .reference()
  //       .child('messages')
  //       .child(userId)
  //       .onValue
  //       .listen(
  //     (chatMessages) {
  //       Map messages = chatMessages.snapshot.value;
  //       if (messages != null) {
  //         messages.forEach((messageKey, messageData) async {
  //           int fav;
  //           try {
  //             // await DBHelper.deleteDB('chat');

  //             messageData['isFavorite'] ? fav = 1 : fav = 0;
  //             await DBHelper.insertData(
  //               tableName: 'chat',
  //               data: {
  //                 'messageId': messageData['id'],
  //                 'userName': messageData['userName'],
  //                 'userId': messageData['userId'],
  //                 'userAvatar': messageData['userAvatar'],
  //                 'userNumber': messageData['userNumber'],
  //                 'createdAt': messageData['createdAt'],
  //                 'message': messageData['message'],
  //                 'emoji': messageData['emoji'],
  //                 'location': messageData['location'],
  //                 'latitude': double.parse(messageData['latitude']),
  //                 'longitude': double.parse(messageData['longitude']),
  //                 'isFavorite': fav,
  //               },
  //             );
  //           } catch (e) {
  //             print('error adding messages from firebase to local db');
  //             print('the error is: $e');
  //           }
  //         });
  //       }
  //     },
  //   );
  // }

  Future<void> sendMessageToFirebaseTest(
      {String message, String emoji, BuildContext context}) async {
    final userDataFromStorage = SaveDataLocally();
    final _userId = await userDataFromStorage.getUserId();
    final _userName = await userDataFromStorage.getUserName();
    final _userAvatar = await userDataFromStorage.getUserAvatar();
    final _userNumber = await userDataFromStorage.getUserPhoneNumber();
    final _location = await getLocationData();
    final url =
        'https://grupo-2020-default-rtdb.firebaseio.com/messages/WVmmJXghS8RR1eCZiCXLTPFY2tL2.json';
    try {
      print(url);
      final response = await http.post(
        url,
        body: json.encode(
          {
            'message': 'message',
            'createdAt': DateTime.now().toString(),
            'userId': _userId,
            'userName': _userName,
            'userAvatar': _userAvatar,
            'userNumber': _userNumber,
            'emoji': emoji,
            'location': _location['address'],
            'longitude': _location['lon'].toString(),
            'latitude': _location['lat'].toString(),
            'isFavorite': false,
            'id': 'kjhkhkjkhk',
          },
        ),
      );
      print(json.decode(response.body));
    } catch (e) {
      print(e);
    }
  }

  Future<void> sendMessageToFirebase(
      {String message, String emoji, BuildContext context}) async {
    try {
      final userDataFromStorage = SaveDataLocally();
      final _userId = await userDataFromStorage.getUserId();
      final _userName = await userDataFromStorage.getUserName();
      final _userAvatar = await userDataFromStorage.getUserAvatar();
      final _userNumber = await userDataFromStorage.getUserPhoneNumber();
      final _location = await getLocationData();

      var ref = FirebaseDatabase.instance
          .reference()
          .child('messages')
          .child(_userId)
          .push();
      ref.set({
        'message': message,
        'createdAt': DateTime.now().toString(),
        'userId': _userId,
        'userName': _userName,
        'userAvatar': _userAvatar,
        'userNumber': _userNumber,
        'emoji': emoji,
        'location': _location['address'],
        'longitude': _location['lon'].toString(),
        'latitude': _location['lat'].toString(),
        'isFavorite': false,
        'id': '',
      }).then((value) {
        ref.update({'id': ref.key});
      });
    } catch (e) {
      print(e.toString());
      print('something got wrong form messages');
    }
  }

  Future<Map<String, dynamic>> getLocationData() async {
    final _location = await Location().getLocation();
    final addressFinder = await Geocoder.local.findAddressesFromCoordinates(
      Coordinates(_location.latitude, _location.longitude),
    );
    final senderAddress = addressFinder.first.addressLine.split(",")[2];
    return {
      'address': senderAddress,
      'lat': _location.latitude,
      'lon': _location.longitude,
    };
  }

  // Future<void> sendPendingMessagesViaHttpRequest() async {
  //   final userId = 'WVmmJXghS8RR1eCZiCXLTPFY2tL2';
  //   final url =
  //       'https://grupo-2020-default-rtdb.firebaseio.com/messages/$userId.json';
  //   var pendingMessage = _chatMessagesList.where(
  //     (message) => message.messageId.trim() == '' && message.userId == userId,
  //   );
  //   pendingMessage.forEach(
  //     (chatMessage) async {
  //       await http.post(
  //         url,
  //         body: json.encode(
  //           {
  //             'message': chatMessage.message,
  //             'createdAt': DateTime.now().toString(),
  //             'userId': userId,
  //             'userName': chatMessage.userName,
  //             'userAvatar': chatMessage.userAvatar,
  //             'emoji': chatMessage.emoji,
  //             'location': chatMessage.location,
  //             'longitude': chatMessage.longitude.toString(),
  //             'latitude': chatMessage.latitude.toString(),
  //             'isFavorite': false,
  //             'id': '',
  //           },
  //         ),
  //       );
  //     },
  //   );
  // }

  Future<void> sendPendingMessages() async {
    final userId = FirebaseAuth.instance.currentUser.uid;
    final _location = await getLocationData();
    final firebaseDatabaseReference = FirebaseDatabase.instance
        .reference()
        .child('messages')
        .child(userId)
        .push();

    var pendingMessage = _chatMessagesList.where(
      (message) => message.messageId.trim() == '',
    );

    [...pendingMessage].forEach((chatMessage) {
      firebaseDatabaseReference.set({
        'message': chatMessage.message,
        'createdAt': DateTime.now().toString(),
        'userId': userId,
        'userName': chatMessage.userName,
        'userAvatar': chatMessage.userAvatar,
        'emoji': chatMessage.emoji,
        'location': chatMessage.location,
        'longitude': chatMessage.longitude.toString(),
        'latitude': chatMessage.latitude.toString(),
        'isFavorite': false,
        'id': '',
      }).then((value) {
        firebaseDatabaseReference.update({
          'id': firebaseDatabaseReference.key,
          'location': _location['address'],
          'longitude': _location['lon'].toString(),
          'latitude': _location['lat'].toString(),
        });
        chatMessage.messageId = firebaseDatabaseReference.key;
      });
    });
  }
}
