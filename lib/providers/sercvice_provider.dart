import 'dart:convert';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:grupo/model/user-model.dart';
import 'package:grupo/save_date_locally_by_sharedPreferences.dart';
import 'package:intl/intl.dart';
import 'package:http/http.dart' as http;

import 'package:grupo/screens/register-screen.dart';
import 'package:grupo/widgets/alert-dialog-widget.dart';
import 'package:location/location.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ServiceProvider with ChangeNotifier {
  static const domain = 'http://2322cf8a2e48.ngrok.io/';
  static const apiContext = 'grupo/';

  GrupoUser userInformations = GrupoUser(
    id: '',
    userName: '',
    userNumber: '',
    avatar: '',
    range: 1,
    latitude: 0.0,
    longitude: 0.0,
    online: true,
    deactivated: false,
    joinDate: DateTime.now(),
  );

  final List<String> avatar = [
    'man-1.png',
    'man-2.png',
    'man-3.png',
    'man-4.png',
    'man-5.png',
    'girl-1.png',
    'girl-2.png',
    'girl-3.png',
    'girl-4.png',
    'girl-5.png',
  ];

  final List<String> imojis = [
    '1.png',
    '2.png',
    '3.png',
    '4.png',
    '5.png',
    '6.png',
    '7.png',
    '8.png',
    '9.png',
    '10.png',
    '11.png',
    '12.png',
    '13.png',
    '14.png',
    '15.png',
    '16.png',
  ];

  String convertDatePreview(DateTime date) {
    final now = DateTime.now();
    final checkDate = DateTime(date.year, date.month, date.day);
    final yesterday = DateTime(now.year, now.month, now.day - 1);

    if (date.day == now.day && date.year == DateTime.now().year) {
      return DateFormat('hh:mm a').format(date);
    } else if (checkDate == yesterday) {
      return 'Yesterday';
    } else if (date.year == DateTime.now().year) {
      return DateFormat('dd MMM').format(date);
    } else {
      return DateFormat('dd MMM yyy').format(date);
    }
  }

  Future<Map<String, dynamic>> getSavedDataAndLocation() async {
    final getLocation = await Location().getLocation();
    final range = await SaveDataLocally().getRangeOfMessages();
    final userName = await SaveDataLocally().getUserName();
    final userNumber = await SaveDataLocally().getUserPhoneNumber();
    final userId = await SaveDataLocally().getUserId();
    final userAvatar = await SaveDataLocally().getUserAvatar();
    final joinDate = await SaveDataLocally().getUserJOinDate();

    return {
      'userName': userName,
      'userNumber': userNumber,
      'userId': userId,
      'userAvatar': userAvatar,
      'range': range,
      'latitude': getLocation.latitude,
      'longitude': getLocation.longitude,
      'joinDate': joinDate,
    };
  }

  Future<void> registerUser() async {
    const action = 'user/register';
    var url = domain + apiContext + action;
    print(url);
    try {
      final getRegisterLocation = await Location().getLocation();
      var body = json.encode({
        'mobile': userInformations.userNumber,
        'nickName': userInformations.userName,
        'iconId': userInformations.avatar,
        'range': userInformations.range,
        'longitude': getRegisterLocation.latitude,
        'latitude': getRegisterLocation.longitude,
        'online': userInformations.online
      });
      final response = await http.post(
        url,
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8'
        },
        body: body,
      );

      final responseData = json.decode(response.body)['payload'];

      print(json.decode(response.body));

      var pref = await SharedPreferences.getInstance();
      pref.setString('userBackendId', responseData['id']);

      userInformations = GrupoUser(
        id: responseData['id'],
        userName: responseData['nickName'],
        userNumber: responseData['mobile'],
        avatar: responseData['mobile'],
        range: responseData['range'],
        latitude: responseData['latitude'],
        longitude: responseData['longitude'],
        online: responseData['online'],
        deactivated: responseData['deactivated'],
        joinDate: DateTime.now(),
      );
    } catch (e) {
      print('error in registering user :');
      print('$e');
    }
  }

  Future<void> updateUserData(GrupoUser newUserStatus) async {
    const action = 'user/update';
    var url = domain + apiContext + action;
    var _backendId = await SaveDataLocally().getUserBackendId();
    var _userNumber = await SaveDataLocally().getUserPhoneNumber();
    print('the id is : $_backendId');
    var body = json.encode({
      "id": _backendId,
      "mobile": _userNumber,
      "range": newUserStatus.range,
      "nickName": newUserStatus.userName,
      "iconId": newUserStatus.avatar,
      "longitude": newUserStatus.longitude,
      "latitude": newUserStatus.latitude,
      "online": newUserStatus.online,
      "deactivated": newUserStatus.deactivated,
    });
    try {
      final response = await http.post(
        url,
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8'
        },
        body: body,
      );
      print(json.decode(response.body));
    } catch (e) {
      print('error in updating user status : $e');
    }
  }

  Future<void> fetchUserRegisterationData() async {
    const action = 'user/register';
    const url = domain + apiContext + action;
    try {
      var response = await http.get(url);
      var data = json.decode(response.body) as Map<String, dynamic>;
      if (data == null) {
        return;
      } else {
        print(data);
      }
    } catch (e) {
      print('>> errorr in fetching user registeration data: $e');
    }
  }

  Future<void> deactivateAccount(String userId, BuildContext context) async {
    final userId = await SaveDataLocally().getUserBackendId();
    var action = 'user/deactivate/$userId';
    var url = domain + apiContext + action;
    print('deactivation url: ');
    print(url);
    try {
      await FirebaseDatabase.instance
          .reference()
          .child('messages')
          .child(userId)
          .remove();

      await FirebaseAuth.instance.signOut();

      // sending diactivation to backEnd
      final response = await http.post(
        url,
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8'
        },
      );
      print(json.decode(response.body));
      print('the status code is : ${response.statusCode}');

      Navigator.of(context).pushReplacementNamed(RegisterScreen.routeName);
    } catch (e) {
      print('error in deactivating account');
      AlertDialogWidget().myAlert(context, e);
    }
  }
}
