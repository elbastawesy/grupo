import 'package:flutter/material.dart';
import 'package:grupo/providers/chat_provider.dart';
import 'package:provider/provider.dart';

class ChatSendMessageControlWidget extends StatefulWidget {
  final Function showEmoji;
  final Function hideEmoji;
  final FocusNode textFieldFoucs;
  // final bool connectedToInternet;

  ChatSendMessageControlWidget(
    this.showEmoji,
    this.hideEmoji,
    this.textFieldFoucs,
    // this.connectedToInternet,
  );
  @override
  _ChatSendMessageControlWidgetState createState() =>
      _ChatSendMessageControlWidgetState();
}

class _ChatSendMessageControlWidgetState
    extends State<ChatSendMessageControlWidget> {
  final _controller = TextEditingController();
  var _sentMessage = '';

  @override
  Widget build(BuildContext context) {
    final chatProvider = Provider.of<ChatProvider>(context);
    return Container(
      height: MediaQuery.of(context).size.height * .1,
      color: Color(0xff735561),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          IconButton(
            icon: Icon(
              Icons.emoji_emotions_outlined,
              size: 30,
              color: Colors.white,
            ),
            onPressed: widget.showEmoji,
          ),
          Expanded(
            child: Container(
              margin: EdgeInsets.symmetric(vertical: 5, horizontal: 10),
              child: TextField(
                controller: _controller,
                focusNode: widget.textFieldFoucs,
                keyboardType: TextInputType.multiline,
                maxLines: null,
                decoration: InputDecoration(
                  contentPadding: EdgeInsets.only(
                    left: 20,
                    right: 25,
                    top: 10,
                    bottom: 10,
                  ),
                  hintText: 'Send a Grupo!',
                  hintStyle: TextStyle(color: Color(0xff8b717c)),
                  fillColor: Colors.white,
                  filled: true,
                  enabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(50),
                  ),
                  focusedBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(50),
                  ),
                ),
                onChanged: (value) {
                  setState(() {
                    _sentMessage = value;
                  });
                },
                onTap: () => widget.hideEmoji(),
              ),
            ),
          ),
          IconButton(
            icon: Icon(Icons.send, size: 30, color: Colors.white),
            onPressed: _sentMessage.trim() != ''
                ? () {
                    // if (widget.connectedToInternet) {
                    chatProvider.sendMessageToFirebase(
                      message: _sentMessage.trim(),
                      emoji: '',
                      context: context,
                    );
                    // }

                    // chatProvider.addChatMessagesToLocalDB(
                    //   message: _sentMessage.trim(),
                    //   emoji: '',
                    //   context: context,
                    // );

                    _controller.clear();
                    setState(() {
                      _sentMessage = '';
                    });
                  }
                : null,
          )
        ],
      ),
    );
  }
}
