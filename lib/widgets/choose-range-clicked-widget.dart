import 'package:flutter/material.dart';

class ChooseRangeClickedWidget extends StatelessWidget {
  final String range;
  final bool changeColor;
  ChooseRangeClickedWidget(this.range, this.changeColor);
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          width: 10,
          height: 30,
          decoration: BoxDecoration(
            color: changeColor ? Color(0xffee8543) : Color(0xffcecece),
            borderRadius: BorderRadius.circular(8),
          ),
        ),
        SizedBox(height: 3),
        Text(
          range,
          style: TextStyle(
            color: Colors.white,
          ),
        ),
      ],
    );
  }
}
