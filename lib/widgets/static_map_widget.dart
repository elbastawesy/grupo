import 'package:flutter/material.dart';
import 'package:google_static_maps_controller/google_static_maps_controller.dart';
import 'package:grupo/constants/google_api_key.dart';

class GoogleStaticMap extends StatefulWidget {
  GoogleStaticMap(this.latitude, this.longitude, this.zoom);
  final double latitude;
  final double longitude;
  final int zoom;
  @override
  _GoogleStaticMapState createState() => _GoogleStaticMapState();
}

class _GoogleStaticMapState extends State<GoogleStaticMap> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: StaticMap(
          width: double.infinity,
          height: double.infinity,
          scaleToDevicePixelRatio: true,
          googleApiKey: GOOGLE_API_KEY,
          zoom: widget.zoom,
          markers: <Marker>[
            Marker(
              color: Theme.of(context).primaryColor,
              label: "A",
              locations: [
                Location(widget.latitude, widget.longitude),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
