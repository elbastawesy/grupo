import 'package:flutter/material.dart';
import 'package:grupo/providers/chat_provider.dart';
import 'package:provider/provider.dart';

class EmojiContainer extends StatelessWidget {
  const EmojiContainer({
    Key key,
    @required this.emoji,
  }) : super(key: key);

  final List<String> emoji;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height * .3,
      width: double.infinity,
      decoration: BoxDecoration(
        image: DecorationImage(
            image: AssetImage('assets/images/BG.png'), fit: BoxFit.cover),
      ),
      child: GridView.builder(
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
            childAspectRatio: 3 / 2,
            crossAxisCount: 3,
            crossAxisSpacing: 5,
            mainAxisSpacing: 10),
        itemCount: emoji.length,
        padding: EdgeInsets.only(top: 20),
        itemBuilder: (ctx, index) => InkWell(
          child: Container(
            margin: EdgeInsets.all(10),
            decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage('assets/images/emoji/${emoji[index]}'),
              ),
            ),
          ),
          onTap: () {
            Provider.of<ChatProvider>(context, listen: false)
                .sendMessageToFirebase(
              message: '',
              emoji: emoji[index],
              context: context,
            );
          },
        ),
      ),
    );
  }
}
