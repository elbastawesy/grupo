import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_database/firebase_database.dart';

import 'package:grupo/model/chat_message_model.dart';
import 'package:grupo/widgets/message_bubble.dart';

class MessageWidget extends StatefulWidget {
  @override
  _MessageWidgetState createState() => _MessageWidgetState();
}

class _MessageWidgetState extends State<MessageWidget> {
  final userId = FirebaseAuth.instance.currentUser.uid;

  bool _checkIfTheUserIsMe(id) {
    return id == userId;
  }

  @override
  Widget build(BuildContext context) {
    final mediaQueryWidth = MediaQuery.of(context).size.width;
    return StreamBuilder(
      stream: FirebaseDatabase.instance
          .reference()
          .child('messages')
          .child(userId)
          .onValue,
      builder: (ctx, chatSnapshot) {
        if (chatSnapshot.hasError) {
          return Center(
            child: Text(chatSnapshot.error),
          );
        }
        if (chatSnapshot.connectionState == ConnectionState.waiting) {
          return Center(
            child: CircularProgressIndicator(),
          );
        }
        if (chatSnapshot.hasData && chatSnapshot.data.snapshot.value != null) {
          Map data = chatSnapshot.data.snapshot.value;
          List messagesList = [];
          data.forEach((key, data) {
            messagesList.add(
              ChatMessage(
                messageId: key,
                createdAt: DateTime.parse(data['createdAt']),
                message: data['message'],
                userName: data['userName'],
                userAvatar: data['userAvatar'],
                userId: data['userId'],
                latitude: double.parse(data['latitude']),
                longitude: double.parse(data['longitude']),
                location: data['location'],
                userNumber: data['userNumber'],
                isFavorite: data['isFavorite'],
                emoji: data['emoji'],
              ),
            );
          });
          messagesList.sort((a, b) => b.createdAt.compareTo(a.createdAt));
          return Column(
            children: [
              Expanded(
                child: ListView.builder(
                    reverse: true,
                    itemCount: messagesList.length,
                    itemBuilder: (ctx, index) {
                      final message = messagesList[index];
                      final _isMe = _checkIfTheUserIsMe(message.userId);
                      return Padding(
                        padding: EdgeInsets.symmetric(
                            horizontal: mediaQueryWidth * .01),
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.baseline,
                          children: [
                            if (!_isMe)
                              Container(
                                width: mediaQueryWidth * .18,
                                child: Image(
                                  image: AssetImage(
                                      'assets/images/avatar/${message.userAvatar}'),
                                ),
                              ),
                            MessageBubble(
                              messageText: message.message,
                              emojiMessage: message.emoji,
                              name: _isMe ? 'Me' : message.userName,
                              time: message.createdAt,
                              location: message.location,
                              isMe: _isMe,
                              mediaQuery: mediaQueryWidth,
                              isFavorite: message.isFavorite,
                              userAvatar: message.userAvatar,
                              userId: userId,
                              messageId: message.messageId,
                            ),
                            if (_isMe)
                              Container(
                                width: mediaQueryWidth * .18,
                                child: Image(
                                  image: AssetImage(
                                      'assets/images/avatar/${message.userAvatar}'),
                                ),
                              ),
                          ],
                        ),
                      );
                    }),
              ),
            ],
          );
        } else if (!chatSnapshot.hasData ||
            chatSnapshot.data.snapshot.value == null) {
          return Center(
            child: Text('welcome to GRUPO'),
          );
        } else {
          return Center(
            child: Text('loading...'),
          );
        }
      },
    );
  }
}
