import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:flutter_chat_bubble/chat_bubble.dart';
import 'package:flutter_chat_bubble/clippers/chat_bubble_clipper_1.dart';
import 'package:flutter_chat_bubble/bubble_type.dart';

import '../model/favorite_messages_model.dart';
import '../providers/sercvice_provider.dart';
import '../providers/favorite_messages_provider.dart';

class MessageBubble extends StatefulWidget {
  MessageBubble({
    this.messageText,
    this.emojiMessage,
    this.name,
    this.time,
    this.location,
    this.isMe,
    this.mediaQuery,
    this.isFavorite,
    this.userId,
    this.userAvatar,
    this.messageId,
  });

  final String messageText;
  final String name;
  final String emojiMessage;
  final double mediaQuery;
  final bool isMe;
  final DateTime time;
  final String location;
  bool isFavorite;
  final String userId;
  final String userAvatar;
  final String messageId;

  @override
  _MessageBubbleState createState() => _MessageBubbleState();
}

class _MessageBubbleState extends State<MessageBubble> {
  @override
  Widget build(BuildContext context) {
    final serviceProvider = Provider.of<ServiceProvider>(context);
    final favoriteMessageProvider =
        Provider.of<FavoriteMessagesProvider>(context);
    var convertedTime = serviceProvider.convertDatePreview(
      widget.time,
    );

    return Container(
      margin: EdgeInsets.symmetric(vertical: 8),
      width: widget.mediaQuery * .8,
      child: Column(
        crossAxisAlignment:
            widget.isMe ? CrossAxisAlignment.end : CrossAxisAlignment.start,
        children: [
          Container(
            margin: const EdgeInsets.only(right: 20, left: 5),
            child: Text(
              widget.name,
              style: TextStyle(color: Color(0xffc93c4c), fontSize: 18),
            ),
          ),
          SizedBox(height: 5),
          LayoutBuilder(
            builder: (ctx, constraints) => Container(
              width: constraints.maxWidth,
              margin: EdgeInsets.only(
                right: constraints.maxWidth * .01,
                left: constraints.maxWidth * .02,
              ),
              child: ChatBubble(
                clipper: widget.isMe
                    ? ChatBubbleClipper1(type: BubbleType.sendBubble)
                    : ChatBubbleClipper1(type: BubbleType.receiverBubble),
                elevation: 2,
                backGroundColor:
                    widget.isMe ? Color(0xffffd8be) : Color(0xffcecece),
                child: Column(
                  children: [
                    buildRowForMessageTextAndFavoriteIcon(
                        constraints,
                        widget.messageText,
                        widget.emojiMessage,
                        widget.isMe,
                        favoriteMessageProvider),
                    buildRowForLocationAndTime(constraints, convertedTime),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

// build row that holds the location and time of message bubble.
  Row buildRowForLocationAndTime(
      BoxConstraints constraints, String convertedTime) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Container(
          margin: const EdgeInsets.only(top: 10),
          padding: EdgeInsets.only(
            left: constraints.maxWidth * .01,
          ),
          child: Text(
            'NearBy: ${widget.location}',
            style: TextStyle(fontSize: 15, color: Color(0xff8b717c)),
          ),
        ),
        Container(
          padding: const EdgeInsets.only(top: 10),
          margin: const EdgeInsets.only(right: 10),
          child: Text(
            '$convertedTime',
            style: TextStyle(
              fontSize: 15,
              color: Color(0xff8b717c),
            ),
          ),
        ),
      ],
    );
  }

// build row that holds the message and icon of message bubble.
  Row buildRowForMessageTextAndFavoriteIcon(
      BoxConstraints constraints,
      String messageText,
      String emojiMessage,
      bool isMe,
      FavoriteMessagesProvider provider) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          padding: EdgeInsets.only(left: constraints.maxWidth * .01),
          width: constraints.maxWidth * .77,
          child: messageText.trim() != ''
              ? Text(
                  messageText,
                  style: TextStyle(
                    fontSize: 18,
                    color: Color(0xff2c0817),
                  ),
                )
              : Image(
                  alignment:
                      isMe ? Alignment.centerRight : Alignment.centerLeft,
                  image: AssetImage('assets/images/emoji/$emojiMessage'),
                  height: 40,
                ),
        ),
        if (!isMe) buildFavoriteIcon(constraints, provider),
      ],
    );
  }

// build star icon to make message as favorite.
  Container buildFavoriteIcon(
      BoxConstraints constraints, FavoriteMessagesProvider provider) {
    var _favoriteMessage = FavoriteMessage(
      message: '',
      id: '',
      saveTime: null,
      userAvatar: '',
      userName: '',
    );
    return Container(
      alignment: Alignment.topRight,
      width: constraints.maxWidth * .08,
      child: InkWell(
        child: widget.isFavorite
            ? Icon(
                Icons.star,
                color: Colors.orangeAccent,
              )
            : Icon(
                Icons.star_border_outlined,
              ),
        onTap: () {
          setState(() {
            widget.isFavorite = !widget.isFavorite;
          });

          provider.makeMessageAsFavoriteAtFirebase(
            userId: widget.userId,
            messageId: widget.messageId,
            isFav: widget.isFavorite,
          );

          if (widget.isFavorite) {
            _favoriteMessage = FavoriteMessage(
              message: widget.messageText,
              id: widget.messageId,
              saveTime: DateTime.now(),
              userAvatar: widget.userAvatar,
              userName: widget.name,
            );

            provider.addFavoriteMessageToLocalDatabase(_favoriteMessage);
          }
        },
      ),
    );
  }
}
