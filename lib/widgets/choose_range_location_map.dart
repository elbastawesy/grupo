import 'package:flutter/material.dart';
import 'package:grupo/widgets/static_map_widget.dart';

class ChooseRangeLocationMap extends StatefulWidget {
  ChooseRangeLocationMap({
    // @required this.getLocationMap,
    @required this.mediaQueryHeight,
    @required this.threeKmCircle,
    @required this.fiveKmCircle,
    @required this.tenKmCircle,
    this.latitude,
    this.longitude,
    this.zoom,
  });

  // final String getLocationMap;
  final double mediaQueryHeight;
  final bool threeKmCircle;
  final bool fiveKmCircle;
  final bool tenKmCircle;
  final double latitude;
  final double longitude;
  final int zoom;

  @override
  _ChooseRangeLocationMapState createState() => _ChooseRangeLocationMapState();
}

class _ChooseRangeLocationMapState extends State<ChooseRangeLocationMap> {
  @override
  Widget build(BuildContext context) {
    return Stack(
      alignment: Alignment.center,
      children: [
        Container(
          height: widget.mediaQueryHeight * .88,
          child:
              GoogleStaticMap(widget.latitude, widget.longitude, widget.zoom),
        ),
        buildCircleAboveMap(context, .35, widget.threeKmCircle),
        buildCircleAboveMap(context, .6, widget.fiveKmCircle),
        buildCircleAboveMap(context, .8, widget.tenKmCircle),
      ],
    );
  }

  Container buildCircleAboveMap(
      BuildContext context, double radius, bool position) {
    return Container(
      height: widget.mediaQueryHeight * radius,
      width: MediaQuery.of(context).size.width * radius,
      decoration: BoxDecoration(
        border: Border.all(
          color: position ? Color(0xffee8543) : Color(0xffcecece),
          width: 6,
        ),
        shape: BoxShape.circle,
      ),
    );
  }
}
