import 'package:flutter/material.dart';
import 'package:grupo/model/user-model.dart';
import 'package:grupo/providers/sercvice_provider.dart';
import 'package:grupo/save_date_locally_by_sharedPreferences.dart';
import 'package:grupo/screens/choose-avatar-screen.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

class UserNameAndUserAvatarWidget extends StatefulWidget {
  UserNameAndUserAvatarWidget({
    this.userName,
    this.userAvatar,
    this.mediaQueryHeight,
  });

  final double mediaQueryHeight;
  String userAvatar;
  String userName;

  @override
  _UserNameAndUserAvatarWidgetState createState() =>
      _UserNameAndUserAvatarWidgetState();
}

class _UserNameAndUserAvatarWidgetState
    extends State<UserNameAndUserAvatarWidget> {
  var controller = TextEditingController();
  var _editingName = false;

  void updateSharedPreferencesData(String name, String avatar) async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    preferences.setString('avatar', avatar);
    preferences.setString('userName', name);
  }

  Future<void> updateUserData(String name, String avatar) async {
    final _joinDate = await SaveDataLocally().getUserJOinDate();
    final serviceProvider =
        Provider.of<ServiceProvider>(context, listen: false);
    var savedData = await serviceProvider.getSavedDataAndLocation();
    var newUserStatus = GrupoUser(
      id: savedData['userId'],
      userName: name,
      avatar: avatar,
      userNumber: savedData['userNumber'],
      range: savedData['range'],
      latitude: savedData['latitude'],
      longitude: savedData['longitude'],
      online: true,
      deactivated: false,
      joinDate: DateTime.parse(_joinDate),
    );
    serviceProvider.userInformations = newUserStatus;

    await serviceProvider.updateUserData(newUserStatus);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: widget.mediaQueryHeight,
      decoration: BoxDecoration(
        image: DecorationImage(
          image: AssetImage('assets/images/profile_BG.png'),
          fit: BoxFit.cover,
        ),
      ),
      child: LayoutBuilder(
        builder: (ctx, constraints) {
          return Column(
            children: [
              Container(
                margin: EdgeInsets.only(top: constraints.maxHeight * .05),
                child: Center(
                  child: Container(
                    child: Stack(
                      alignment: Alignment.topCenter,
                      children: [
                        Container(
                          height: constraints.maxHeight * .6,
                          child: Image(
                            image: AssetImage(
                                'assets/images/avatar/${widget.userAvatar}'),
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(
                            left: MediaQuery.of(context).size.width * .5,
                            top: constraints.maxHeight * .1,
                          ),
                          child: RaisedButton(
                            shape: CircleBorder(),
                            onPressed: () async {
                              var result =
                                  await Navigator.of(context).pushNamed(
                                ChooseAvatarScreen.routeName,
                                arguments: widget.userAvatar,
                              );
                              setState(() {
                                widget.userAvatar = result.toString();
                              });

                              updateUserData(
                                widget.userName,
                                result.toString(),
                              );

                              updateSharedPreferencesData(
                                widget.userName,
                                result.toString(),
                              );
                            },
                            child: Icon(Icons.edit),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              SizedBox(height: constraints.maxHeight * .1),
              Container(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  mainAxisSize: MainAxisSize.max,
                  children: [
                    _editingName
                        ? Container(
                            child: Expanded(
                              child: Container(
                                padding: EdgeInsets.only(
                                    left: constraints.maxWidth * .1),
                                child: TextField(
                                  controller: controller
                                    ..text = widget.userName,
                                  style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 25,
                                  ),
                                  decoration: InputDecoration(
                                    enabledBorder: UnderlineInputBorder(
                                      borderSide: BorderSide(
                                        style: BorderStyle.solid,
                                        color: Colors.white,
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          )
                        : buildNameViewer(constraints.maxWidth * .5),
                    SizedBox(width: constraints.maxWidth * .02),
                    _editingName
                        ? Container(
                            height: constraints.maxHeight * .15,
                            decoration: BoxDecoration(
                              border: Border.all(
                                color: Colors.white30,
                                width: 5,
                              ),
                              shape: BoxShape.circle,
                            ),
                            child: IconButton(
                              icon: Icon(
                                Icons.check,
                                color: Colors.white30,
                                size: 22,
                              ),
                              onPressed: () {
                                if (controller.text.isNotEmpty) {
                                  widget.userName = controller.text.trim();

                                  updateUserData(
                                    controller.text.trim(),
                                    widget.userAvatar,
                                  );

                                  updateSharedPreferencesData(
                                    controller.text.trim(),
                                    widget.userAvatar,
                                  );
                                  setState(() {
                                    _editingName = false;
                                  });
                                }
                              },
                            ),
                          )
                        : Container(
                            height: constraints.maxHeight * .15,
                            decoration: BoxDecoration(
                              border: Border.all(
                                color: Colors.white30,
                                width: 5,
                              ),
                              shape: BoxShape.circle,
                            ),
                            child: IconButton(
                              icon: Icon(
                                Icons.edit,
                                color: Colors.white30,
                                size: 25,
                              ),
                              onPressed: () {
                                setState(() {
                                  _editingName = true;
                                });
                              },
                            ),
                          ),
                  ],
                ),
              ),
            ],
          );
        },
      ),
    );
  }

  buildNameViewer(double textWidth) {
    if (widget.userName.length < 10) {
      return Container(
        child: Text(
          widget.userName,
          style: TextStyle(
            color: Colors.white,
            fontSize: 25,
          ),
          textAlign: TextAlign.center,
        ),
      );
    } else {
      return Container(
        width: textWidth,
        child: FittedBox(
          child: Text(
            widget.userName,
            style: TextStyle(
              color: Colors.white,
              fontSize: 25,
            ),
            textAlign: TextAlign.center,
          ),
        ),
      );
    }
  }
}
