import 'package:flutter/material.dart';
import 'package:grupo/providers/chat_provider.dart';
import 'package:grupo/save_date_locally_by_sharedPreferences.dart';
import 'package:grupo/widgets/message_bubble.dart';
import 'package:provider/provider.dart';

class OfflineMessages extends StatefulWidget {
  @override
  _OfflineMessagesState createState() => _OfflineMessagesState();
}

class _OfflineMessagesState extends State<OfflineMessages> {
  var _userId = '';

  Future<void> _getUserId() async {
    final dataFromLocalStorage = SaveDataLocally();
    var userId = await dataFromLocalStorage.getUserId();
    setState(() {
      this._userId = userId;
    });
  }

  _checkIfTheUserIsMe(id) {
    return id == _userId;
  }

  @override
  void initState() {
    super.initState();
    _getUserId();
  }

  @override
  Widget build(BuildContext context) {
    var chatProviderMessages = Provider.of<ChatProvider>(context).getChat;
    final mediaQueryWidth = MediaQuery.of(context).size.width;
    return StreamBuilder(
      stream: chatProviderMessages,
      builder: (context, snapshot) {
        if (snapshot.hasError) {
          print(snapshot.error);
        }
        if (snapshot.connectionState == ConnectionState.waiting) {
          return Center(
            child: CircularProgressIndicator(),
          );
        }
        if (!snapshot.hasData || snapshot.data.length == 0) {
          return Center(
            child: Text('welcome to GRUPO'),
          );
        }
        if (snapshot.hasData && snapshot.data != null) {
          return ListView.builder(
            reverse: true,
            itemCount: snapshot.data.length,
            itemBuilder: (ctx, index) {
              final message = snapshot.data[index];
              final _isMe = _checkIfTheUserIsMe(message.userId);
              return Padding(
                padding:
                    EdgeInsets.symmetric(horizontal: mediaQueryWidth * .01),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.baseline,
                  children: [
                    if (!_isMe)
                      Container(
                        width: mediaQueryWidth * .18,
                        child: Image(
                          image: AssetImage(
                              'assets/images/avatar/${message.userAvatar}'),
                        ),
                      ),
                    MessageBubble(
                      messageText: message.message,
                      emojiMessage: message.emoji,
                      name: _isMe ? 'Me' : message.userName,
                      time: message.createdAt,
                      location: message.location,
                      isMe: _isMe,
                      mediaQuery: mediaQueryWidth,
                      isFavorite: message.isFavorite,
                      userAvatar: message.userAvatar,
                      userId: message.userId,
                      messageId: message.messageId,
                    ),
                    if (_isMe)
                      Container(
                        width: mediaQueryWidth * .18,
                        child: Image(
                          image: AssetImage(
                              'assets/images/avatar/${message.userAvatar}'),
                        ),
                      ),
                  ],
                ),
              );
            },
          );
        }

        return Center(
          child: Text('loading...'),
        );
      },
    );
  }
}
