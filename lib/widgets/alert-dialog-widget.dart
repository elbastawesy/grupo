import 'package:flutter/material.dart';

class AlertDialogWidget {
  Future<void> myAlert(BuildContext ctx, String content) async {
    return showDialog(
      context: ctx,
      builder: (BuildContext context) => AlertDialog(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(20),
        ),
        content: Container(
          child: Text(
            content,
            style: TextStyle(
              color: Color(0xff2c0817),
              fontSize: 14,
            ),
          ),
        ),
        actions: [
          Container(
            width: MediaQuery.of(context).size.width * .9,
            child: Column(
              children: [
                Divider(
                  color: Color(0xff2c0817),
                  thickness: 1.5,
                ),
                Center(
                  child: InkWell(
                    onTap: () {
                      Navigator.of(context).pop();
                    },
                    child: Text(
                      'Ok',
                      style: TextStyle(
                        color: Color(0xff2c0817),
                        fontSize: 20,
                      ),
                    ),
                  ),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
