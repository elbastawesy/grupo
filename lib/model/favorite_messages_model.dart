class FavoriteMessage {
  FavoriteMessage({
    this.userName,
    this.userAvatar,
    this.message,
    this.saveTime,
    this.id,
  });
  final String userName;
  final String userAvatar;
  final String message;
  final DateTime saveTime;
  final String id;
}
