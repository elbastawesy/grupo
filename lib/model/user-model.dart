import 'package:flutter/material.dart';

class GrupoUser {
  GrupoUser({
    @required this.id,
    @required this.userName,
    @required this.avatar,
    @required this.userNumber,
    @required this.range,
    @required this.latitude,
    @required this.longitude,
    @required this.online,
    @required this.deactivated,
    @required this.joinDate,
  });

  String id;
  String userNumber;
  String avatar;
  int range;
  String userName;
  double longitude;
  double latitude;
  bool online;
  bool deactivated;
  DateTime joinDate;
}
