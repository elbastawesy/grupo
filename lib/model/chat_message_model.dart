class ChatMessage {
  ChatMessage({
    this.message,
    this.userName,
    this.userNumber,
    this.userAvatar,
    this.createdAt,
    this.location,
    this.messageId,
    this.userId,
    this.latitude,
    this.longitude,
    this.isFavorite,
    this.emoji,
  });
  final String message;
  final String userAvatar;
  final String userName;
  final String userNumber;
  final DateTime createdAt;
  String messageId;
  final String location;
  String userId;
  final double latitude;
  final double longitude;
  final bool isFavorite;
  final String emoji;
}
