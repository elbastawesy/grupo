import 'package:country_code_picker/country_code_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:grupo/constants/messages.dart';
import '../constants/regex_constants.dart';

import 'OTP_screen.dart';

class RegisterScreen extends StatefulWidget {
  static const routeName = '/register';

  @override
  _RegisterScreenState createState() => _RegisterScreenState();
}

class _RegisterScreenState extends State<RegisterScreen> {
  String _countryCode = '';
  final _formKey = GlobalKey<FormState>();
  var phoneNumber = '';
  var showCountryCode = false;

  @override
  Widget build(BuildContext context) {
    final mediaQuery = MediaQuery.of(context).size.height;

    return Scaffold(
      body: buildRegisterScreenBody(
        context,
        mediaQuery,
      ),
    );
  }

  Container buildRegisterScreenBody(BuildContext context, double mediaQuery) {
    return Container(
      height: double.infinity,
      width: double.infinity,
      decoration: BoxDecoration(
        image: DecorationImage(
          image: AssetImage('assets/images/BG.png'),
          fit: BoxFit.cover,
        ),
      ),
      child: GestureDetector(
        onTap: () {
          FocusScope.of(context).unfocus();
          setState(() {
            showCountryCode = false;
          });
        },
        child: Center(
          child: SingleChildScrollView(
            child: Column(
              children: [
                SizedBox(height: mediaQuery * .1),
                // this container for app icon
                Container(
                  height: mediaQuery * .3,
                  child: Image(
                    image: AssetImage('assets/images/appIcon.png'),
                  ),
                ),
                // container contains app name
                Container(
                  height: mediaQuery * .1,
                  child: Text(
                    'Grupo',
                    style: TextStyle(
                      fontSize: 50,
                      color: Color(0xffd04f54),
                    ),
                  ),
                ),
                SizedBox(height: mediaQuery * .25),
                // container contains the form for adding mobile number
                buildRegisterForm(mediaQuery, this.phoneNumber),
              ],
            ),
          ),
        ),
      ),
    );
  }

  void saveForm() {
    final _isValid = _formKey.currentState.validate();
    if (!_isValid) {
      return;
    }
    _formKey.currentState.save();
    Navigator.of(context).push(
      MaterialPageRoute(
        builder: (context) => OTPScreen(phoneNumber, _countryCode),
      ),
    );
    print(this._countryCode + this.phoneNumber);
  }

  String validatePhoneNumber(String phoneNumber) {
    if (phoneNumber.isEmpty) {
      return FIELD_IS_EMPTY_ERROR_MESSAGE;
    }
    if (!numberRegex.hasMatch(phoneNumber)) {
      return CORRECT_NUMBER_ERROR_MESSAGE;
    }

    return null;
  }

  Container buildRegisterForm(double mediaQuery, String userNumber) {
    return Container(
      height: mediaQuery * .25,
      color: Colors.white.withOpacity(.1),
      child: Form(
        key: _formKey,
        // this is custom widget RegisterInputForm()
        child: LayoutBuilder(
          builder: (ctx, constraints) => Column(
            children: [
              Container(
                child: Column(
                  children: [
                    Container(
                      margin: EdgeInsets.only(
                        top: constraints.maxHeight * 0.1,
                        left: 20,
                        right: 20,
                      ),
                      child: buildRegisterFormTextFormField(),
                    ),
                    // SizedBox(height: constraints.maxHeight * .2),
                    buildRegisterFormRaisedButton(constraints),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  TextFormField buildRegisterFormTextFormField() {
    return TextFormField(
      inputFormatters: [
        FilteringTextInputFormatter.deny(
          preventLettersRegex,
        ),
      ],
      maxLength: 10,
      validator: this.validatePhoneNumber,
      style: TextStyle(fontSize: 18),
      onTap: () {
        setState(() {
          showCountryCode = true;
        });
      },
      decoration: InputDecoration(
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(50),
        ),
        contentPadding: showCountryCode ? null : EdgeInsets.only(left: 30),
        counterText: '',
        filled: true,
        fillColor: Color(0xffc0b3ba),
        hintText: 'Phone Number',
        errorStyle: TextStyle(
          fontSize: 10,
          color: Colors.white,
        ),
        prefix: showCountryCode
            ? Container(
                height: MediaQuery.of(context).size.width * .05,
                width: MediaQuery.of(context).size.width * .2,
                child: CountryCodePicker(
                  textStyle: TextStyle(fontSize: 18),
                  showDropDownButton: true,
                  showFlagMain: false,
                  showCountryOnly: true,
                  dialogTextStyle: TextStyle(letterSpacing: 1),
                  // dialogSize: Size(20, 30),
                  initialSelection: 'EG',
                  onInit: (code) {
                    this._countryCode = code.toString();
                  },
                  favorite: ['+20'],
                  onChanged: (code) {
                    setState(() {
                      this._countryCode = code.toString();
                    });
                  },
                ),
              )
            : null,
      ),
      keyboardType: TextInputType.phone,
      onSaved: (value) {
        setState(() {
          this.phoneNumber = value;
        });
      },
    );
  }

  Container buildRegisterFormRaisedButton(BoxConstraints constraints) {
    return Container(
      margin: EdgeInsets.only(
        top: constraints.maxHeight * .08,
        right: 20,
        left: 20,
        bottom: constraints.maxHeight * .05,
      ),
      width: double.infinity,
      child: RaisedButton(
        color: Color(0xffd75a57),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(50),
        ),
        padding: const EdgeInsets.all(10),
        child: Text(
          'Register',
          style: TextStyle(
            fontSize: 20,
            color: Colors.white,
          ),
        ),
        onPressed: () {
          this.saveForm();
        },
      ),
    );
  }
}
