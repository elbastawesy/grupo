import 'package:flutter/material.dart';
import 'package:grupo/constants/messages.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:flutter_chat_bubble/bubble_type.dart';
import 'package:flutter_chat_bubble/chat_bubble.dart';
import 'package:flutter_chat_bubble/clippers/chat_bubble_clipper_1.dart';

import '../providers/favorite_messages_provider.dart';

class MyGrupoAndFavoriteScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final deviceWidth = MediaQuery.of(context).size.width;
    final favoriteMessagesData =
        Provider.of<FavoriteMessagesProvider>(context, listen: false);
    return Scaffold(
      appBar: AppBar(
        title: Text('My Grupo'),
        centerTitle: true,
        leading: FlatButton(
          child: null,
          onPressed: null,
        ),
      ),
      body: FutureBuilder(
          future: Provider.of<FavoriteMessagesProvider>(context, listen: false)
              .fetchFavoriteMessages(),
          builder: (ctx, snapShot) {
            final favoriteListLength =
                favoriteMessagesData.getMessagesList.length;
            return snapShot.connectionState == ConnectionState.waiting
                ? Center(
                    child: CircularProgressIndicator(),
                  )
                : favoriteListLength <= 0
                    ? Center(child: Text(FAVORITE_MESSAGES_IS_EMPTY))
                    : ListView.builder(
                        itemCount: favoriteListLength,
                        itemBuilder: (context, index) {
                          final _favoriteMessage =
                              favoriteMessagesData.getMessagesList[index];
                          return Container(
                            padding: EdgeInsets.symmetric(
                                horizontal: deviceWidth * .01, vertical: 15),
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.baseline,
                              children: [
                                Container(
                                  width: deviceWidth * .14,
                                  child: Image(
                                    image: AssetImage(
                                        'assets/images/avatar/${_favoriteMessage.userAvatar}'),
                                  ),
                                ),
                                Container(
                                  width: deviceWidth * .8,
                                  margin: EdgeInsets.symmetric(
                                      horizontal: deviceWidth * .02),
                                  child: Row(
                                    children: [
                                      Column(
                                        children: [
                                          Container(
                                            width: deviceWidth * .7,
                                            child: Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment
                                                      .spaceBetween,
                                              children: [
                                                Text(
                                                  _favoriteMessage.userName,
                                                  style: TextStyle(
                                                    fontSize: 16,
                                                    color: Color(0xffc93c4c),
                                                  ),
                                                ),
                                                Text(
                                                  DateFormat('hh:mm').format(
                                                    _favoriteMessage.saveTime,
                                                  ),
                                                  style: TextStyle(
                                                    fontSize: 14,
                                                    color: Color(0xff8b717c),
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                          Container(
                                            width: deviceWidth * .7,
                                            child: ChatBubble(
                                              elevation: 2,
                                              clipper: ChatBubbleClipper1(
                                                type: BubbleType.receiverBubble,
                                              ),
                                              backGroundColor:
                                                  Color(0xffcecece),
                                              child: Container(
                                                width: deviceWidth * .7,
                                                child: Text(
                                                  _favoriteMessage.message,
                                                  style: TextStyle(
                                                    fontSize: 16,
                                                    color: Color(0xff2c0817),
                                                  ),
                                                ),
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                      Container(
                                        width: deviceWidth * .1,
                                        child: Icon(
                                          Icons.star,
                                          color: Colors.orangeAccent,
                                          size: 40,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          );
                        });
          }),
    );
  }
}
