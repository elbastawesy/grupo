import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:grupo/model/user-model.dart';
import 'package:grupo/providers/notification-provider.dart';
import 'package:grupo/providers/sercvice_provider.dart';
import 'package:provider/provider.dart ';
import 'package:grupo/constants/messages.dart';
import 'package:grupo/widgets/alert-dialog-widget.dart';
import 'package:location/location.dart';

import './grupo-chat-screen.dart';
import 'favorite_grupos_screen.dart';
import './my-profile-screen.dart';
import './notifications-screen.dart';
import './more_screen.dart';

class MainScreen extends StatefulWidget {
  static const routeName = '/main-screen';
  @override
  _MainScreenState createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen> with WidgetsBindingObserver {
  var _currentIndex = 0;
  FlutterLocalNotificationsPlugin localNotifications =
      FlutterLocalNotificationsPlugin();

  List<Widget> _tabs = [
    GrupoChatScreen(),
    MyProfileScreen(),
    MyGrupoAndFavoriteScreen(),
    NotificationsScreen(),
    SettingsScreen(),
  ];

  Future<void> checkIfLocationPermissionDisabled() async {
    try {
      Location location = Location();
      bool _serviceEnabled;
      PermissionStatus _permissionGranted;
      LocationData _locationData;

      _serviceEnabled = await location.serviceEnabled();

      if (!_serviceEnabled) {
        _serviceEnabled = await location.requestService();
        if (!_serviceEnabled) {
          return;
        }
      }

      _permissionGranted = await location.hasPermission();

      if (_permissionGranted == PermissionStatus.denied) {
        _permissionGranted = await location.requestPermission();

        if (_permissionGranted != PermissionStatus.granted) {
          return AlertDialogWidget()
              .myAlert(context, LOCATION_PERMISSION_DENIED_ERROR);
        }
      }
      _locationData = await location.getLocation();
    } catch (e) {
      AlertDialogWidget().myAlert(context, e);
    }
  }

  Future<void> updateUserDataWhenAppLifeCycleChanges(bool online) async {
    final serviceProvider =
        Provider.of<ServiceProvider>(context, listen: false);
    final savedData = await serviceProvider.getSavedDataAndLocation();

    var newUserStatus = GrupoUser(
      id: savedData['userId'],
      userName: savedData['userName'],
      userNumber: savedData['userNumber'],
      avatar: savedData['userAvatar'],
      range: savedData['range'],
      latitude: savedData['latitude'],
      longitude: savedData['longitude'],
      joinDate: DateTime.parse(savedData['joinDate']),
      online: online,
      deactivated: false,
    );

    serviceProvider.userInformations = newUserStatus;
    await serviceProvider.updateUserData(newUserStatus);
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    super.didChangeAppLifecycleState(state);

    if (state == AppLifecycleState.paused) {
      print('app paused');
      updateUserDataWhenAppLifeCycleChanges(false);
    } else if (state == AppLifecycleState.inactive) {
      updateUserDataWhenAppLifeCycleChanges(false);
    } else {
      print('app resumed');
      updateUserDataWhenAppLifeCycleChanges(true);
    }
  }

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addObserver(this);
    updateUserDataWhenAppLifeCycleChanges(true);
    checkIfLocationPermissionDisabled();

    var androidInitialize = AndroidInitializationSettings("ic_launcher");
    var iOSInitialize = IOSInitializationSettings();
    var initializeSettings = InitializationSettings(
      android: androidInitialize,
      iOS: iOSInitialize,
    );
    localNotifications.initialize(initializeSettings);

    Provider.of<NotificationProvider>(context, listen: false)
        .receivePushNotification(localNotifications);
  }

  @override
  void dispose() {
    super.dispose();
    WidgetsBinding.instance.removeObserver(this);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _tabs[_currentIndex],
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: _currentIndex,
        backgroundColor: Theme.of(context).primaryColor,
        type: BottomNavigationBarType.fixed,
        selectedFontSize: 10,
        unselectedFontSize: 10,
        iconSize: 30,
        selectedItemColor: Color(0xff4f2b3b),
        unselectedItemColor: Colors.white54,
        selectedLabelStyle: TextStyle(fontWeight: FontWeight.bold),
        elevation: 5,
        items: [
          BottomNavigationBarItem(
              icon: Icon(Icons.chat_bubble_outline),
              label: 'Grupo Now!',
              activeIcon: Icon(Icons.chat_bubble)),
          BottomNavigationBarItem(
            icon: Icon(Icons.person_outline),
            label: 'Profile',
            activeIcon: Icon(Icons.person),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.favorite_border_outlined),
            label: 'Grupos',
            activeIcon: Icon(Icons.favorite),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.notifications_none),
            label: 'Notifications',
            activeIcon: Icon(Icons.notifications),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.more_horiz),
            label: '',
          ),
        ],
        onTap: (index) {
          setState(() {
            _currentIndex = index;
          });
        },
      ),
    );
  }
}
