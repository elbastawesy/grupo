import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:grupo/constants/messages.dart';
import 'package:grupo/model/user-model.dart';
import 'package:grupo/providers/sercvice_provider.dart';
import 'package:grupo/save_date_locally_by_sharedPreferences.dart';
import 'package:grupo/widgets/alert-dialog-widget.dart';
import 'package:provider/provider.dart';

import './choose-avatar-screen.dart';
import './choose-range-screen.dart';

class AddProfileDataScreen extends StatefulWidget {
  static const routeName = '/add-profile-data';
  final String phoneNumber;
  AddProfileDataScreen(this.phoneNumber);

  @override
  _AddProfileDataScreenState createState() => _AddProfileDataScreenState();
}

class _AddProfileDataScreenState extends State<AddProfileDataScreen> {
  bool loadingAvatar;
  var avatarName = 'man-1.png';
  final _textFieldController = TextEditingController();
  GrupoUser _personData;
  String name = '';

  @override
  void initState() {
    loadingAvatar = false;
    _personData = GrupoUser(
      id: '',
      userName: '',
      userNumber: widget.phoneNumber,
      avatar: this.avatarName,
      range: 1,
      deactivated: false,
      latitude: 0.0,
      longitude: 0.0,
      online: true,
      joinDate: DateTime.now(),
    );
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final mediaQuery = MediaQuery.of(context).size.height;

    return WillPopScope(
      onWillPop: _onWillPop,
      child: Scaffold(
        appBar: buildAppBar(),
        body: Container(
          height: double.infinity,
          width: double.infinity,
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage('assets/images/BG.png'),
              fit: BoxFit.cover,
            ),
          ),
          child: SingleChildScrollView(
            padding: const EdgeInsets.all(30),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                // this container contains avatar image and a button for changing avatar
                Container(
                  margin: EdgeInsets.only(top: mediaQuery * .02),
                  child: Stack(
                    children: [
                      buildChosenAvatarContainer(mediaQuery),
                      // this is a button for changing avatar
                      Container(
                        margin: EdgeInsets.only(
                          left: MediaQuery.of(context).size.width * .48,
                          top: mediaQuery * .01,
                        ),
                        child: RaisedButton(
                          shape: CircleBorder(),
                          onPressed: () async {
                            final result = await Navigator.of(context)
                                .pushNamed(ChooseAvatarScreen.routeName,
                                    arguments: avatarName);

                            this.avatarName = result.toString();

                            setState(() {
                              loadingAvatar = true;
                            });
                          },
                          child: Icon(Icons.edit),
                        ),
                      ),
                    ],
                  ),
                ),
                SizedBox(height: mediaQuery * .1),
                buildUserNicknameTextField(context),
              ],
            ),
          ),
        ),
      ),
    );
  }

  AppBar buildAppBar() {
    return AppBar(
      title: Text('Add Your Profile'),
      centerTitle: true,
      leading: null,
      actions: [
        FlatButton(
          onPressed: () {
            saveForm();
            // saveUserOnFirebase();
          },
          child: Text(
            'Done',
            style: TextStyle(color: Colors.white, fontSize: 18),
          ),
        )
      ],
    );
  }

  Container buildChosenAvatarContainer(double mediaQuery) {
    return Container(
      height: mediaQuery * .225,
      decoration: BoxDecoration(
        image: DecorationImage(
          image: !loadingAvatar
              ? AssetImage(
                  'assets/images/avatar/man-1.png',
                )
              : AssetImage(
                  'assets/images/avatar/$avatarName',
                ),
          fit: BoxFit.contain,
        ),
        shape: BoxShape.circle,
      ),
    );
  }

  Container buildUserNicknameTextField(BuildContext context) {
    var maxLength = 20;
    return Container(
      width: MediaQuery.of(context).size.width * .8,
      child: TextField(
        maxLengthEnforced: false,
        controller: _textFieldController,
        style: TextStyle(
          fontSize: 18,
          color: Colors.black,
        ),
        autocorrect: false,
        inputFormatters: [
          LengthLimitingTextInputFormatter(10),
        ],
        decoration: InputDecoration(
          hintText: 'Nickname',
          filled: true,
          counterText: '',
          fillColor: Color(0xffc0b3ba),
          errorStyle: TextStyle(color: Colors.white),
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(50),
          ),
          contentPadding: EdgeInsets.only(left: 30),
        ),
        onChanged: (value) {
          if (value.length <= maxLength) {
            _personData.userName = value.trim();
          } else {
            AlertDialogWidget().myAlert(context, NICKNAME_LENGTH_ERROR_MESSAGE);
            _textFieldController.text = value.trim().substring(0, 9);
            return;
          }
        },
      ),
    );
  }

  Future<bool> _onWillPop() async {
    return (await showDialog(
          context: context,
          builder: (context) => AlertDialog(
            title: Text('Are you sure?'),
            content: Text('Do you want to exit Grupo'),
            actions: <Widget>[
              FlatButton(
                onPressed: () => Navigator.of(context).pop(false),
                child: Text('No'),
              ),
              FlatButton(
                onPressed: () => Navigator.of(context).pop(true),
                child: Text('Yes'),
              ),
            ],
          ),
        )) ??
        false;
  }

  Future<void> saveForm() async {
    final serviceProvider =
        Provider.of<ServiceProvider>(context, listen: false);
    if (_textFieldController.text.isEmpty) {
      AlertDialogWidget().myAlert(context, NICKNAME_ERROR_MESSAGE);
      return;
    }

    _personData = GrupoUser(
      id: '',
      userName: _textFieldController.text.trim(),
      userNumber: widget.phoneNumber,
      avatar: this.avatarName,
      range: 1,
      latitude: 0.0,
      longitude: 0.0,
      online: true,
      deactivated: false,
      joinDate: _personData.joinDate,
    );

    serviceProvider.userInformations = _personData;

    SaveDataLocally().setUserData(
      userName: _textFieldController.text.trim(),
      avatar: '${this.avatarName}',
      joinDate: _personData.joinDate.toString(),
      phoneNumber: widget.phoneNumber,
      userId: FirebaseAuth.instance.currentUser.uid,
    );
    await serviceProvider.registerUser();
    Navigator.of(context).pushNamed(ChooseRangeScreen.routeName);
  }
}
