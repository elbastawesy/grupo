import 'package:flutter/material.dart';
import 'package:grupo/constants/messages.dart';
import 'package:grupo/providers/sercvice_provider.dart';
import 'package:provider/provider.dart';

import '../widgets/alert-dialog-widget.dart';

class ChooseAvatarScreen extends StatefulWidget {
  static const routeName = '/choose-avatar';

  @override
  _ChooseAvatarScreenState createState() => _ChooseAvatarScreenState();
}

class _ChooseAvatarScreenState extends State<ChooseAvatarScreen> {
  var avatarName = '';
  int selectedCard = -1;

  @override
  Widget build(BuildContext context) {
    final avatarData = Provider.of<ServiceProvider>(context).avatar;
    var oldAvatarName = ModalRoute.of(context).settings.arguments;

    return Scaffold(
        appBar: buildAppBar(context),
        body: WillPopScope(
          onWillPop: () async {
            Navigator.of(context).pop(oldAvatarName);
            return true;
          },
          child: GridView.builder(
            padding: const EdgeInsets.only(top: 20),
            gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
              crossAxisCount: 3,
              childAspectRatio: 1.2,
              crossAxisSpacing: MediaQuery.of(context).size.width * .07,
              mainAxisSpacing: MediaQuery.of(context).size.width * .07,
            ),
            itemCount: avatarData.length,
            itemBuilder: (BuildContext context, int index) => InkWell(
              child: Container(
                height: MediaQuery.of(context).size.height * .3,
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  image: DecorationImage(
                    image:
                        AssetImage('assets/images/avatar/${avatarData[index]}'),
                  ),
                ),
                child: selectedCard == index
                    ? Container(
                        alignment: Alignment.topRight,
                        child: Icon(
                          Icons.check_box_rounded,
                          color: Color(0xffd75a56),
                          size: 35,
                        ),
                      )
                    : null,
              ),
              onTap: () {
                avatarName = avatarData[index];
                setState(() {
                  selectedCard = index;
                });
              },
            ),
          ),
        ));
  }

  AppBar buildAppBar(BuildContext context) {
    final oldAvatarName = ModalRoute.of(context).settings.arguments;
    return AppBar(
      title: Text('Choose Avatar'),
      centerTitle: true,
      leading: IconButton(
        icon: Icon(Icons.arrow_back_ios),
        onPressed: () {
          Navigator.of(context).pop(oldAvatarName);
        },
      ),
      actions: [buildAppBarActionButton(context)],
    );
  }

  FlatButton buildAppBarActionButton(BuildContext context) {
    return FlatButton(
      onPressed: () {
        if (avatarName == '') {
          return AlertDialogWidget().myAlert(
            context,
            AVATAR_NAMe_ERROR_MESSAGE,
          );
        }
        Navigator.of(context).pop(avatarName);
      },
      child: Text(
        'Done',
        style: TextStyle(color: Colors.white, fontSize: 16),
      ),
    );
  }
}
