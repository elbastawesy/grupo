import 'package:connectivity/connectivity.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:grupo/screens/register-screen.dart';
import 'package:grupo/widgets/alert-dialog-widget.dart';
import 'package:pinput/pin_put/pin_put.dart';
import './add-profile-data-screen.dart';

class OTPScreen extends StatefulWidget {
  static const routeName = '/confirm-code';
  final String phoneNumber, countryCode;

  OTPScreen(this.phoneNumber, this.countryCode);

  @override
  _OTPScreenState createState() => _OTPScreenState();
}

class _OTPScreenState extends State<OTPScreen> {
  String fullPhoneNumber;
  String _verificationCode;
  final TextEditingController _pinPutController = TextEditingController();
  final FocusNode _pinPutFocusNode = FocusNode();
  final BoxDecoration pinPutDecoration = BoxDecoration(
    color: const Color.fromRGBO(43, 46, 66, 1),
    borderRadius: BorderRadius.circular(10.0),
    border: Border.all(
      color: const Color.fromRGBO(126, 203, 224, 1),
    ),
  );

  Future<void> checkConnectivity(BuildContext ctx) async {
    var connectivityResult = await (Connectivity().checkConnectivity());
    if (connectivityResult == ConnectivityResult.none) {
      return showDialog(
          context: ctx,
          builder: (BuildContext context) => AlertDialog(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(20),
                ),
                content: Text(
                  'no internet connection. please check it.',
                  style: TextStyle(
                    color: Color(0xff2c0817),
                    fontSize: 14,
                  ),
                ),
                actions: [
                  Container(
                    width: MediaQuery.of(context).size.width * .9,
                    child: Column(
                      children: [
                        Divider(color: Color(0xff2c0817), thickness: 1.5),
                        Center(
                          child: InkWell(
                            onTap: () {
                              Navigator.pushReplacement(
                                  context,
                                  MaterialPageRoute(
                                    builder: (ctx) => RegisterScreen(),
                                  ));
                            },
                            child: Text(
                              'Ok',
                              style: TextStyle(
                                  color: Color(0xff2c0817), fontSize: 20),
                            ),
                          ),
                        ),
                      ],
                    ),
                  )
                ],
              ));
    }
  }

  @override
  void initState() {
    super.initState();
    fullPhoneNumber = widget.countryCode + widget.phoneNumber;
    checkConnectivity(context);
    try {
      _verifyPhone();
    } catch (e) {
      print('something got wrong form OTP');
    }
  }

  @override
  void dispose() {
    super.dispose();
    _pinPutController.dispose();
    _pinPutFocusNode.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Confirm Code'),
        centerTitle: true,
        leading: IconButton(
          icon: Icon(Icons.arrow_back_ios),
          onPressed: () {
            Navigator.of(context).pop();
          },
        ),
      ),
      body: buildBody(context),
    );
  }

  buildBody(BuildContext context) {
    return Container(
      height: double.infinity,
      width: double.infinity,
      decoration: BoxDecoration(
        image: DecorationImage(
          image: AssetImage('assets/images/BG.png'),
          fit: BoxFit.cover,
        ),
      ),
      child: Column(
        children: [
          buildMobileInfoText(),
          buildAddingCodeWidget(context),
        ],
      ),
    );
  }

  Container buildMobileInfoText() {
    return Container(
      margin: EdgeInsets.only(top: MediaQuery.of(context).size.height * .05),
      child: Text(
        'Verifying: $fullPhoneNumber',
        style: TextStyle(
          color: Colors.white,
          fontSize: 20,
        ),
      ),
    );
  }

  Padding buildAddingCodeWidget(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(
        top: MediaQuery.of(context).size.height * .1,
        left: MediaQuery.of(context).size.width * .07,
        right: MediaQuery.of(context).size.width * .07,
      ),
      child: PinPut(
        fieldsCount: 6,
        textStyle: const TextStyle(fontSize: 25.0, color: Colors.white),
        eachFieldWidth: 40.0,
        eachFieldHeight: 55.0,
        focusNode: _pinPutFocusNode,
        controller: _pinPutController,
        submittedFieldDecoration: pinPutDecoration,
        selectedFieldDecoration: pinPutDecoration,
        followingFieldDecoration: pinPutDecoration,
        pinAnimationType: PinAnimationType.fade,
        onSubmit: (pin) async {
          onAddingOTP(pin);
        },
      ),
    );
  }

  onAddingOTP(pin) async {
    try {
      await FirebaseAuth.instance
          .signInWithCredential(PhoneAuthProvider.credential(
              verificationId: _verificationCode, smsCode: pin))
          .then((value) async {
        if (value.user != null) {
          Navigator.pushAndRemoveUntil(
              context,
              MaterialPageRoute(
                builder: (context) => AddProfileDataScreen(fullPhoneNumber),
              ),
              (route) => false);
        }
      });
    } catch (e) {
      AlertDialogWidget().myAlert(context, e);
      _pinPutController.text = '';
    }
  }

  _verifyPhone() async {
    await FirebaseAuth.instance.verifyPhoneNumber(
      phoneNumber: '$fullPhoneNumber',
      verificationCompleted: completeVerification,
      verificationFailed: verificationFailed,
      codeSent: sendCodeToMobile,
      codeAutoRetrievalTimeout: codeAutoRetrivalTimeout,
      timeout: Duration(seconds: 60),
    );
  }

  void codeAutoRetrivalTimeout(String verificationID) {
    if (this.mounted) {
      setState(() {
        _verificationCode = verificationID;
      });
    }
  }

  void completeVerification(PhoneAuthCredential credential) async {
    await FirebaseAuth.instance
        .signInWithCredential(credential)
        .then((value) async {
      if (value.user != null) {
        Navigator.pushAndRemoveUntil(
            context,
            MaterialPageRoute(
              builder: (context) => AddProfileDataScreen(fullPhoneNumber),
            ),
            (route) => false);
      }
    });
  }

  void sendCodeToMobile(String verficationID, int resendToken) {
    setState(() {
      _verificationCode = verficationID;
    });
  }

  void verificationFailed(FirebaseAuthException e) {
    print(e.message);
  }
}
