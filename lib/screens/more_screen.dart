import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:grupo/constants/messages.dart';
import 'package:grupo/screens/allow_receiving_notifications_screen.dart';
import 'package:grupo/screens/invite_friends_screen.dart';
import 'package:grupo/screens/privacy_policy_screen.dart';

class SettingsScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('More'),
        centerTitle: true,
        leading: FlatButton(
          child: null,
          onPressed: null,
        ),
      ),
      body: ListView(
        children: [
          ListTile(
            leading: Icon(Icons.notifications,
                color: Theme.of(context).primaryColor),
            title: Text(
              'Notifications',
              style: TextStyle(fontSize: 18),
            ),
            trailing: Icon(
              Icons.arrow_forward_ios,
              size: 15,
            ),
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (ctx) => AllowReceivingNotificationsScreen(),
                ),
              );
            },
          ),
          Padding(
            padding: const EdgeInsets.only(left: 8, right: 8),
            child: Divider(),
          ),
          ListTile(
            leading: Icon(
              Icons.privacy_tip,
              color: Theme.of(context).primaryColor,
            ),
            title: Text(
              'Privacy Policy',
              style: TextStyle(fontSize: 18),
            ),
            trailing: Icon(
              Icons.arrow_forward_ios,
              size: 15,
            ),
            onTap: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (ctx) => PrivacyPolicyScreen(),
                  ));
            },
          ),
          Padding(
            padding: const EdgeInsets.only(left: 8, right: 8),
            child: Divider(),
          ),
          ListTile(
            leading: Icon(
              Icons.person_add,
              color: Theme.of(context).primaryColor,
            ),
            title: Text(
              'Invite friends',
              style: TextStyle(fontSize: 18),
            ),
            trailing: Icon(
              Icons.arrow_forward_ios,
              size: 15,
            ),
            onTap: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (ctx) => InviteFriendsScreen(),
                  ));
            },
          ),
          Padding(
            padding: const EdgeInsets.only(left: 8, right: 8),
            child: Divider(),
          ),
          ListTile(
            leading: Icon(Icons.exit_to_app_rounded,
                color: Theme.of(context).primaryColor),
            title: Text(
              'Exit Grupo',
              style: TextStyle(fontSize: 18),
            ),
            trailing: Icon(
              Icons.arrow_forward_ios,
              size: 15,
            ),
            onTap: () {
              onExit(context);
            },
          ),
        ],
      ),
    );
  }

  void onExit(BuildContext context) {
    showDialog(
        context: context,
        builder: (ctx) => AlertDialog(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20),
              ),
              content: Text(
                EXIT_GRUPO_MESSAGE,
                style: TextStyle(
                  color: Color(0xff2c0817),
                  fontSize: 14,
                ),
              ),
              actions: [
                FlatButton(
                  onPressed: () async {
                    SystemNavigator.pop();
                  },
                  child: Text(
                    'Yes',
                    style: TextStyle(color: Color(0xff2c0817), fontSize: 16),
                  ),
                ),
                FlatButton(
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                  child: Text(
                    'No',
                    style: TextStyle(color: Color(0xff2c0817), fontSize: 16),
                  ),
                ),
              ],
            ));
  }
}
