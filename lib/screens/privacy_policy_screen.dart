import 'package:flutter/material.dart';
import 'package:grupo/constants/privacy_policy_content.dart';

class PrivacyPolicyScreen extends StatelessWidget {
  static const routeName = '/privacy-policy';
  @override
  Widget build(BuildContext context) {
    final _deviceWidth = MediaQuery.of(context).size.width;
    return Scaffold(
      appBar: AppBar(
        title: Text('Privacy Policy'),
        centerTitle: true,
        leading: IconButton(
          icon: Icon(Icons.arrow_back_ios),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
      ),
      body: SingleChildScrollView(
        padding:
            EdgeInsets.symmetric(horizontal: _deviceWidth * .03, vertical: 15),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              alignment: Alignment.center,
              child: Text(
                'PRIVACY POLICY',
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
            SizedBox(height: 15),
            Text(INTRODUCTION, textAlign: TextAlign.justify),
            buildTitle('1. INFORMATION WE COLLECT'),
            Text(INFORMATION_WE_COLLECT, textAlign: TextAlign.justify),
            buildSubTitle(INFORMATION_WE_COLLECT_1, _deviceWidth),
            SizedBox(height: 5),
            Padding(
              padding: EdgeInsets.only(left: _deviceWidth * .08),
              child: Column(
                children: [
                  Text(INFORMATION_WE_COLLECT_1_1,
                      textAlign: TextAlign.justify),
                  SizedBox(height: 5),
                  Text(INFORMATION_WE_COLLECT_1_2,
                      textAlign: TextAlign.justify),
                ],
              ),
            ),
            SizedBox(height: 5),
            Text(INFORMATION_WE_COLLECT_1_END, textAlign: TextAlign.justify),
            buildSubTitle(INFORMATION_WE_COLLECT_2, _deviceWidth),
            buildSubTitle(INFORMATION_WE_COLLECT_3, _deviceWidth),
            buildSubTitle(INFORMATION_WE_COLLECT_4, _deviceWidth),
            buildSubTitle(INFORMATION_WE_COLLECT_5, _deviceWidth),
            buildSubTitle(INFORMATION_WE_COLLECT_6, _deviceWidth),
            SizedBox(height: 5),
            Text(INFORMATION_WE_COLLECT_6_END, textAlign: TextAlign.justify),
            buildSubTitle(INFORMATION_WE_COLLECT_7_1, _deviceWidth),
            SizedBox(height: 5),
            Padding(
              padding: EdgeInsets.only(left: _deviceWidth * .04),
              child: Text(INFORMATION_WE_COLLECT_7_2,
                  textAlign: TextAlign.justify),
            ),
            SizedBox(height: 5),
            Text(INFORMATION_WE_COLLECT_7_END, textAlign: TextAlign.justify),
            buildTitle('2. SHARING PERSONAL INFORMATION WITH THIRD PARTIES'),
            Text(SHARING_PERSONAL_INFORMATION),
            buildSubTitle(SHARING_PERSONAL_INFORMATION_1, _deviceWidth),
            buildSubTitle(SHARING_PERSONAL_INFORMATION_2, _deviceWidth),
            buildSubTitle(SHARING_PERSONAL_INFORMATION_3, _deviceWidth),
            buildSubTitle(SHARING_PERSONAL_INFORMATION_4, _deviceWidth),
            buildSubTitle(SHARING_PERSONAL_INFORMATION_5, _deviceWidth),
            buildSubTitle(SHARING_PERSONAL_INFORMATION_6, _deviceWidth),
            buildSubTitle(SHARING_PERSONAL_INFORMATION_7, _deviceWidth),
            buildTitle('3. YOUR CHOICES REGARDING YOUR INFORMATION'),
            buildSubTitle(CHOISE_READING_INFORMATION_1, _deviceWidth),
            buildSubTitle(CHOISE_READING_INFORMATION_2, _deviceWidth),
            buildTitle('4. SECURITY AND STORAGE OF INFORMATION'),
            Text(SECURITY_1, textAlign: TextAlign.justify),
            SizedBox(height: 5),
            Text(SECURITY_2, textAlign: TextAlign.justify),
            buildTitle('5. LINKS TO THIRD-PARTY WEBSITES'),
            Text(LINKS_TO_THIRD_PARTY, textAlign: TextAlign.justify),
            buildTitle('6. OUR POLICY TOWARDS CHILDREN'),
            Text(OUR_POLICY_TOWARDS_CHILDREN, textAlign: TextAlign.justify),
            buildTitle('7. CALIFORNIA DO-NOT-TRACK DISCLOSURE'),
            Text(CALIFORNIA_DO_NOT_TRACK_DISCLOSURE,
                textAlign: TextAlign.justify),
            buildTitle('8. UPDATES TO THIS PRIVACY POLICY'),
            Text(UPDATES_TO_THIS_PRIVACY_POLICY, textAlign: TextAlign.justify),
            SizedBox(height: 15),
            Text(MODIFIED_PRIVACY, textAlign: TextAlign.justify),
            SizedBox(height: 15),
            Text(CONTACT_PRIVACY, textAlign: TextAlign.justify),
          ],
        ),
      ),
    );
  }

  buildTitle(String title) {
    return Column(
      children: [
        SizedBox(height: 10),
        Text(
          title,
          style: TextStyle(fontWeight: FontWeight.bold),
        ),
        SizedBox(height: 5),
      ],
    );
  }

  buildSubTitle(String subTitle, double mediaQuery) {
    return Column(
      children: [
        SizedBox(height: 5),
        Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              width: mediaQuery * .02,
              margin: EdgeInsets.only(
                top: 5,
                right: mediaQuery * .01,
                left: mediaQuery * .01,
              ),
              alignment: Alignment.center,
              child: Container(
                width: 5,
                height: 5,
                decoration: BoxDecoration(
                  color: Colors.black,
                  shape: BoxShape.circle,
                ),
              ),
            ),
            Container(
              width: mediaQuery * .88,
              child: Wrap(
                children: [
                  Text(subTitle, textAlign: TextAlign.justify),
                ],
              ),
            ),
          ],
        ),
      ],
    );
  }
}
