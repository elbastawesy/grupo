import 'dart:async';

import 'package:flutter/material.dart';
import 'package:grupo/model/user-model.dart';
import 'package:grupo/providers/sercvice_provider.dart';
import 'package:grupo/save_date_locally_by_sharedPreferences.dart';
import 'package:location/location.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../widgets/alert-dialog-widget.dart';
import '../widgets/choose_range_location_map.dart';
import '../constants/messages.dart';
import '../widgets/choose-range-clicked-widget.dart';
import './main-screen.dart';

class ChooseRangeScreen extends StatefulWidget {
  static const routeName = '/choose-range';

  @override
  _ChooseRangeScreenState createState() => _ChooseRangeScreenState();
}

class _ChooseRangeScreenState extends State<ChooseRangeScreen> {
  var _changeToOneKmRangeAndColor = false;
  var _changeToThreeKmRangeAndColor = false;
  var _changeToFiveKmRangeAndColor = false;
  var _changeToTenKmRangeAndColor = false;
  var _navigate = true;
  var _gpsPermissoin = false;
  int messageRange;

  // String _getLocationMap;
  int zoom;

  double latitude = 1;
  double longitude = 1;

  Future<void> getLocation() async {
    try {
      Location location = Location();
      bool _serviceEnabled;
      PermissionStatus _permissionGranted;
      LocationData _locationData;

      _serviceEnabled = await location.serviceEnabled();

      if (!_serviceEnabled) {
        _serviceEnabled = await location.requestService();
        if (!_serviceEnabled) {
          return;
        }
      }

      _permissionGranted = await location.hasPermission();

      if (_permissionGranted == PermissionStatus.denied) {
        _permissionGranted = await location.requestPermission();

        if (_permissionGranted != PermissionStatus.granted) {
          return AlertDialogWidget()
              .myAlert(context, LOCATION_PERMISSION_DENIED_ERROR);
        }
      }

      _locationData = await location.getLocation();
      setState(() {
        latitude = _locationData.latitude;
        longitude = _locationData.longitude;
        _gpsPermissoin = true;
      });
    } catch (e) {
      AlertDialogWidget().myAlert(context, e);
    }
  }

  Future<void> updateMessageRange(int range) async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    preferences.setInt('range', range);

    final serviceProvider =
        Provider.of<ServiceProvider>(context, listen: false);

    final savedData = await serviceProvider.getSavedDataAndLocation();
    serviceProvider.userInformations = GrupoUser(
      id: savedData['userId'],
      userName: savedData['userName'],
      userNumber: savedData['userNumber'],
      avatar: savedData['userAvatar'],
      range: range,
      latitude: savedData['latitude'],
      longitude: savedData['longitude'],
      online: savedData['online'],
      joinDate: DateTime.parse(savedData['joinDate']),
      deactivated: false,
    );
  }

  Future<void> getMessagesRange() async {
    var localData = SaveDataLocally();
    messageRange = await localData.getRangeOfMessages();
    updateRangeClickedButton(messageRange);
  }

  void updateRangeClickedButton(int range) {
    if (range == 1) {
      setState(() {
        _changeToOneKmRangeAndColor = true;
      });
    } else if (range == 3) {
      setState(() {
        _changeToThreeKmRangeAndColor = true;
      });
    } else if (range == 5) {
      _changeToFiveKmRangeAndColor = true;
    } else {
      setState(() {
        _changeToTenKmRangeAndColor = true;
      });
    }
  }

  @override
  void initState() {
    super.initState();
    getMessagesRange();
    setState(() {
      zoom = 16;
    });
    getLocation();
  }

  @override
  Widget build(BuildContext context) {
    final appBar = AppBar(
      title: Text('Choose Range'),
      centerTitle: true,
      leading: IconButton(
        icon: Icon(null),
        onPressed: null,
      ),
      actions: [
        AppBarAction(
          navigate: _navigate,
          gpsPermissoin: _gpsPermissoin,
          getLocationFunction: getLocation,
        ),
      ],
    );

    final mediaQueryHeight = MediaQuery.of(context).size.height -
        MediaQuery.of(context).padding.top -
        appBar.preferredSize.height;

    return Scaffold(
      appBar: appBar,
      body: Column(
        children: [
          Container(
            height: mediaQueryHeight * .12,
            color: Color(0xff735561),
            child: LayoutBuilder(
              builder: (ctx, constraints) => Container(
                padding: EdgeInsets.all(constraints.maxHeight * .12),
                child: Stack(
                  children: [
                    Container(
                      margin:
                          EdgeInsets.only(top: constraints.maxHeight * 0.13),
                      child: Divider(
                        color: Color(0xffa3a3a3),
                        indent: constraints.maxWidth * .03,
                        endIndent: constraints.maxWidth * .04,
                        thickness: 3,
                      ),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        InkWell(
                          child: ChooseRangeClickedWidget(
                              '1 km', _changeToOneKmRangeAndColor),
                          onTap: () {
                            updateMessageRange(1);
                            setState(() {
                              _changeToOneKmRangeAndColor = true;
                              _changeToThreeKmRangeAndColor = false;
                              _changeToFiveKmRangeAndColor = false;
                              _changeToTenKmRangeAndColor = false;
                              _navigate = true;
                              zoom = 16;
                            });
                          },
                        ),
                        InkWell(
                          child: ChooseRangeClickedWidget(
                              '3 km', _changeToThreeKmRangeAndColor),
                          onTap: () {
                            updateMessageRange(3);
                            setState(() {
                              _changeToOneKmRangeAndColor = false;
                              _changeToThreeKmRangeAndColor = true;
                              _changeToFiveKmRangeAndColor = false;
                              _changeToTenKmRangeAndColor = false;
                              _navigate = true;
                              zoom = 14;
                            });
                          },
                        ),
                        InkWell(
                          child: ChooseRangeClickedWidget(
                              '5 km', _changeToFiveKmRangeAndColor),
                          onTap: () {
                            updateMessageRange(5);
                            setState(() {
                              setState(() {
                                _changeToOneKmRangeAndColor = false;
                                _changeToThreeKmRangeAndColor = false;
                                _changeToFiveKmRangeAndColor = true;
                                _changeToTenKmRangeAndColor = false;
                                _navigate = true;
                                zoom = 13;
                              });
                            });
                          },
                        ),
                        InkWell(
                          child: ChooseRangeClickedWidget(
                              '10 km', _changeToTenKmRangeAndColor),
                          onTap: () {
                            updateMessageRange(10);
                            setState(() {
                              setState(() {
                                _changeToOneKmRangeAndColor = false;
                                _changeToThreeKmRangeAndColor = false;
                                _changeToFiveKmRangeAndColor = false;
                                _changeToTenKmRangeAndColor = true;
                                _navigate = true;
                                zoom = 12;
                              });
                            });
                          },
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ),
          ChooseRangeLocationMap(
            // getLocationMap: _getLocationMap,
            mediaQueryHeight: mediaQueryHeight,
            threeKmCircle: _changeToThreeKmRangeAndColor,
            fiveKmCircle: _changeToFiveKmRangeAndColor,
            tenKmCircle: _changeToTenKmRangeAndColor,
            latitude: latitude,
            longitude: longitude,
            zoom: zoom,
          ),
        ],
      ),
    );
  }
}

class AppBarAction extends StatelessWidget {
  AppBarAction({this.navigate, this.gpsPermissoin, this.getLocationFunction});

  final bool navigate;
  final bool gpsPermissoin;
  final Function getLocationFunction;

  @override
  Widget build(BuildContext context) {
    return FlatButton(
      onPressed: () {
        if (!navigate) {
          return AlertDialogWidget()
              .myAlert(context, CHOOSE_RANGE__ERROR_MESSAGE);
        }
        if (!gpsPermissoin) {
          return showDialog(
            context: context,
            builder: (ctx) => AlertDialog(
              content: Text(ENABLE_SERVICE_ERROR_MESSAGE),
              actions: [
                FlatButton(
                    onPressed: () {
                      getLocationFunction();
                      Navigator.pop(context);
                    },
                    child: Text('OK'))
              ],
            ),
          );
        }
        Navigator.of(context).pushNamed(MainScreen.routeName);
      },
      child: Text(
        'Next',
        style: TextStyle(color: Colors.white, fontSize: 16),
      ),
    );
  }
}
