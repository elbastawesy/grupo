import 'package:flutter/material.dart';
import 'package:grupo/save_date_locally_by_sharedPreferences.dart';
import 'package:shared_preferences/shared_preferences.dart';

class AllowReceivingNotificationsScreen extends StatefulWidget {
  @override
  _AllowReceivingNotificationsScreenState createState() =>
      _AllowReceivingNotificationsScreenState();
}

class _AllowReceivingNotificationsScreenState
    extends State<AllowReceivingNotificationsScreen> {
  bool loading = true;
  bool _allowNotification;

  getAllowNotifications() async {
    var getSavedData = SaveDataLocally();
    var allowNotifications = await getSavedData.allowNotifications();
    _allowNotification = allowNotifications;
    setState(() {
      loading = false;
    });
  }

  @override
  void initState() {
    super.initState();
    getAllowNotifications();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Grupo'),
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back_ios,
          ),
          onPressed: () {
            Navigator.of(context).pop();
          },
        ),
      ),
      body: loading
          ? Center(
              child: CircularProgressIndicator(),
            )
          : Container(
              child: ListTile(
                leading: Icon(Icons.notifications,
                    color: Theme.of(context).primaryColor),
                title: Text(
                  'Allow Notifications',
                  style: TextStyle(fontSize: 18),
                ),
                trailing: Switch.adaptive(
                  value: _allowNotification,
                  onChanged: (value) async {
                    SharedPreferences preferences =
                        await SharedPreferences.getInstance();
                    preferences.setBool('allowNotifications', value);
                    setState(() {
                      _allowNotification = !_allowNotification;
                    });
                  },
                ),
              ),
            ),
    );
  }
}
