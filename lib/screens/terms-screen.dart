import 'package:flutter/material.dart';
import 'package:grupo/constants/terms_of_use.dart';

class TermsScreen extends StatelessWidget {
  static const routeName = '/terms-of-use';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Terms of Use'),
        centerTitle: true,
        leading: IconButton(
          icon: Icon(Icons.arrow_back_ios),
          onPressed: () {
            Navigator.of(context).pop();
          },
        ),
      ),
      body: SingleChildScrollView(
        padding: const EdgeInsets.all(15),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              alignment: Alignment.center,
              child: Text(
                'TERMS OF USE',
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
            SizedBox(height: 15),
            Text(TERMS_INTRODUCTION, textAlign: TextAlign.start),
            SizedBox(height: 5),
            Text(TERMS_SECOND_INTRODUCTION, textAlign: TextAlign.justify),
            SizedBox(height: 10),
            Text('1- OUR SERVICES',
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                )),
            SizedBox(height: 5),
            Text(TERMS_SERVICE),
            Padding(
              padding: const EdgeInsets.only(left: 20),
              child: Column(
                children: [
                  Text('- $SERVICE_1', textAlign: TextAlign.justify),
                  SizedBox(height: 5),
                  Text('- $SERVICE_2', textAlign: TextAlign.justify),
                  Padding(
                    padding: const EdgeInsets.only(left: 20),
                    child: Column(
                      children: [
                        Text('a- $SERVICE_2_A', textAlign: TextAlign.justify),
                        SizedBox(height: 5),
                        Text('b- $SERVICE_2_B', textAlign: TextAlign.justify),
                        SizedBox(height: 5),
                        Text('c- $SERVICE_2_C', textAlign: TextAlign.justify),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(height: 5),
            Text(SERVICE_END),
            SizedBox(height: 10),
            Text('2- USER CONTENT',
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                )),
            SizedBox(height: 5),
            Text(USER_CONTENT_1, textAlign: TextAlign.justify),
            SizedBox(height: 5),
            Text(USER_CONTENT_2, textAlign: TextAlign.justify),
            SizedBox(height: 5),
            Text(USER_CONTENT_3, textAlign: TextAlign.justify),
            SizedBox(height: 5),
            Text(USER_CONTENT_4, textAlign: TextAlign.justify),
            SizedBox(height: 10),
            Text('3- USER CONTENT LICENSE GRANT',
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                )),
            SizedBox(height: 5),
            Text(USER_CONTENT_LICENSE_GRANT, textAlign: TextAlign.justify),
            SizedBox(height: 10),
            Text('4- GRUPO MOBILE APP',
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                )),
            SizedBox(height: 5),
            Text('a- $GURPO_MOBILE_APP_A_1', textAlign: TextAlign.justify),
            SizedBox(height: 5),
            Text(GURPO_MOBILE_APP_A_1, textAlign: TextAlign.justify),
            SizedBox(height: 5),
            Text('b- $GURPO_MOBILE_APP_B', textAlign: TextAlign.justify),
            SizedBox(height: 10),
            Text('5- $OUR_PROPRIETARY_RIGHTS', textAlign: TextAlign.justify),
            SizedBox(height: 10),
            Text('6- $PRIVACY', textAlign: TextAlign.justify),
            SizedBox(height: 10),
            Text('7- $SECURITY', textAlign: TextAlign.justify),
            SizedBox(height: 10),
            Text('8- $DMCA_NOTICE', textAlign: TextAlign.justify),
            SizedBox(height: 5),
            Text(DMCA_NOTICE_1, textAlign: TextAlign.justify),
            SizedBox(height: 5),
            Padding(
              padding: const EdgeInsets.only(left: 20),
              child: Column(
                children: [
                  Text('- $DMCA_NOTICE_1_1', textAlign: TextAlign.justify),
                  SizedBox(height: 5),
                  Text('- $DMCA_NOTICE_1_2', textAlign: TextAlign.justify),
                  SizedBox(height: 5),
                  Text('- $DMCA_NOTICE_1_3', textAlign: TextAlign.justify),
                  SizedBox(height: 5),
                  Text('- $DMCA_NOTICE_1_4', textAlign: TextAlign.justify),
                  SizedBox(height: 5),
                  Text('- $DMCA_NOTICE_1_5', textAlign: TextAlign.justify),
                  SizedBox(height: 5),
                  Text('- $DMCA_NOTICE_1_6', textAlign: TextAlign.justify),
                  SizedBox(height: 5),
                ],
              ),
            ),
            Text(DMCA_NOTICE_2, textAlign: TextAlign.justify),
            SizedBox(height: 5),
            Text(DMCA_NOTICE_3, textAlign: TextAlign.justify),
            SizedBox(height: 5),
            Text(DMCA_NOTICE_4, textAlign: TextAlign.justify),
            SizedBox(height: 10),
            Text('9- $THIRD_PARTY_LINKS_AND_INFORMATION',
                textAlign: TextAlign.justify),
            SizedBox(height: 10),
            Text('10- $INDEMNITY', textAlign: TextAlign.justify),
            SizedBox(height: 10),
            Text('11- $NO_WARRANTY', textAlign: TextAlign.justify),
            SizedBox(height: 5),
            Text(NO_WARRRANTY_2, textAlign: TextAlign.justify),
            SizedBox(height: 5),
            Text(NO_WARRRANTY_3, textAlign: TextAlign.justify),
            SizedBox(height: 10),
            Text('12- $LIMITATION_OF_LIABILITY_1',
                textAlign: TextAlign.justify),
            SizedBox(height: 5),
            Text(LIMITATION_OF_LIABILITY_2, textAlign: TextAlign.justify),
            SizedBox(height: 5),
            Text(LIMITATION_OF_LIABILITY_3, textAlign: TextAlign.justify),
            SizedBox(height: 5),
            Text(LIMITATION_OF_LIABILITY_4, textAlign: TextAlign.justify),
            SizedBox(height: 10),
            Text(
                '13- GOVERNING LAW, ARBITRATION, AND CLASS ACTION/JURY TRIAL WAIVER',
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                )),
            SizedBox(height: 5),
            Text('a- $GOVERNING_LAW_A', textAlign: TextAlign.justify),
            SizedBox(height: 5),
            Text('b- $GOVERNING_LAW_B', textAlign: TextAlign.justify),
            SizedBox(height: 5),
            Text('c- $GOVERNING_LAW_C', textAlign: TextAlign.justify),
            SizedBox(height: 10),
            Text('14- GENERAL', textAlign: TextAlign.justify),
            SizedBox(height: 5),
            Text('a- $GENERAL_A', textAlign: TextAlign.justify),
            SizedBox(height: 5),
            Text('b- $GENERAL_B', textAlign: TextAlign.justify),
            SizedBox(height: 5),
            Text('c- $GENERAL_C', textAlign: TextAlign.justify),
            SizedBox(height: 5),
            Text('d- $GENERAL_D', textAlign: TextAlign.justify),
            SizedBox(height: 5),
            Text('e- $GENERAL_E', textAlign: TextAlign.justify),
            SizedBox(height: 20),
            Text(MODIFIED, textAlign: TextAlign.justify),
            SizedBox(height: 15),
            Container(
              child: FlatButton.icon(
                label: Text('BACK'),
                textColor: Theme.of(context).primaryColor,
                icon: Icon(
                  Icons.arrow_back_ios,
                  size: 20,
                ),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
            )
          ],
        ),
      ),
    );
  }
}
