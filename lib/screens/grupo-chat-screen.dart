import 'dart:async';

import 'package:flutter/material.dart';
import 'package:grupo/model/user-model.dart';
import 'package:grupo/save_date_locally_by_sharedPreferences.dart';
import 'package:provider/provider.dart';

import 'package:grupo/providers/sercvice_provider.dart';
import 'package:grupo/screens/choose-range-screen.dart';
import 'package:grupo/widgets/chat_send_message_control_widget.dart';
import 'package:grupo/widgets/message_widget.dart';
import '../widgets/emojis_container.dart';

class GrupoChatScreen extends StatefulWidget {
  static const routeName = '/chat-screen';
  @override
  _GrupoChatScreenState createState() => _GrupoChatScreenState();
}

class _GrupoChatScreenState extends State<GrupoChatScreen> {
  var _loadingPage = true;
  var _emoji = false;
  var _textFieldFocus = FocusNode();
  // StreamSubscription<ConnectivityResult> listenToConnection;

  void _changeEmojeState() {
    _textFieldFocus.unfocus();
    setState(() {
      _emoji = !_emoji;
    });
  }

  void _hideEmojiContainer() {
    setState(() {
      _emoji = false;
    });
  }

  // checkIfConnectedToInternet() async {
  //   var connectivityResult = await (Connectivity().checkConnectivity());
  //   if (connectivityResult == ConnectivityResult.mobile ||
  //       connectivityResult == ConnectivityResult.wifi) {
  //     print('connected to internet');
  //   } else {
  //     Provider.of<ChatProvider>(context, listen: false)
  //         .fetchChatMessagedFromLocalDB();
  //     setState(() {
  //       connectedToInternet = false;
  //       _loadingPage = false;
  //     });
  //   }
  // }

  // @override
  // void initState() {
  //   super.initState();
  //   checkIfConnectedToInternet();
  //   listenToConnection = Connectivity().onConnectivityChanged.listen((event) {
  //     if (event == ConnectivityResult.wifi ||
  //         event == ConnectivityResult.mobile) {
  //       setState(() {
  //         this.connectedToInternet = true;
  //       });
  //       Provider.of<ChatProvider>(context, listen: false).sendPendingMessages();
  //       setState(() {
  //         _loadingPage = false;
  //       });
  //       Provider.of<ChatProvider>(context, listen: false)
  //           .fetchFromFirebaseAndAddingToLocalDB();
  //     }
  //   });
  // }

  // @override
  // void dispose() {
  //   super.dispose();
  //   listenToConnection.cancel();
  // }
  // var userInfo = GrupoUser(
  //   name: '',
  //   number: '',
  //   avatar: '',
  //   joinDate: DateTime.now(),
  // );
  //
  Future<void> fetchUserInfoFromLocalStorage() async {
    final serviceProvider =
        Provider.of<ServiceProvider>(context, listen: false);
    final range = await SaveDataLocally().getRangeOfMessages();
    final savedData = await serviceProvider.getSavedDataAndLocation();
    serviceProvider.userInformations = GrupoUser(
      id: savedData['userId'],
      userName: savedData['userName'],
      userNumber: savedData['userNumber'],
      avatar: savedData['userAvatar'],
      range: range,
      latitude: savedData['latitude'],
      longitude: savedData['longitude'],
      online: savedData['online'],
      joinDate: DateTime.parse(savedData['joinDate']),
      deactivated: false,
    );
  }

  @override
  void initState() {
    super.initState();
    setState(() {});
    fetchUserInfoFromLocalStorage();
  }

  @override
  Widget build(BuildContext context) {
    final emoji = Provider.of<ServiceProvider>(context).imojis;
    return Scaffold(
      appBar: AppBar(
        title: Text('Grupo Now!'),
        centerTitle: true,
        leading: FlatButton(
          child: null,
          onPressed: null,
        ),
        actions: [
          IconButton(
            icon: Icon(Icons.location_on_outlined),
            onPressed: () {
              Navigator.of(context).pushNamed(ChooseRangeScreen.routeName);
            },
          ),
        ],
      ),
      // floatingActionButton: FloatingActionButton(
      //   onPressed: () async {
      //     var test =
      //         Provider.of<ServiceProvider>(context, listen: false).userStatus;
      //     var date = await SaveDataLocally().getRangeOfMessages();
      //     print(test.userName);
      //     print(test.userNumber);
      //     print(test.avatar);
      //     print(test.joinDate);
      //     print(test.range);
      //     print(date);
      //     print(DateTime.now().toString());
      //   },
      //   child: Text('ok'),
      // ),
      body: Column(
        children: [
          Expanded(
              child:
                  //  _loadingPage
                  //     ? Center(child: CircularProgressIndicator())
                  //     : connectedToInternet
                  //         ?
                  MessageWidget()
              // : OfflineMessages(),
              ),
          ChatSendMessageControlWidget(
            _changeEmojeState,
            _hideEmojiContainer,
            _textFieldFocus,
          ),
          _emoji ? EmojiContainer(emoji: emoji) : Container(),
        ],
      ),
    );
  }
}
