import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:grupo/providers/notification-provider.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:provider/provider.dart';
import 'package:grupo/providers/sercvice_provider.dart';

class NotificationsScreen extends StatefulWidget {
  static const routName = '/notification-screen';
  @override
  _NotificationsScreenState createState() => _NotificationsScreenState();
}

class _NotificationsScreenState extends State<NotificationsScreen> {
  final userId = FirebaseAuth.instance.currentUser.uid;

  @override
  Widget build(BuildContext context) {
    final serviceProvider = Provider.of<ServiceProvider>(context);
    return Scaffold(
      appBar: AppBar(
        title: Text('Notifications'),
        centerTitle: true,
        leading: FlatButton(
          child: null,
          onPressed: null,
        ),
        actions: [
          FlatButton(
            child: Text(
              'Clear all',
              style: TextStyle(color: Colors.white, fontSize: 16),
            ),
            onPressed: () {
              try {
                Provider.of<NotificationProvider>(context, listen: false)
                    .clearNotificatons();
              } catch (e) {
                print(e);
              }
            },
          )
        ],
      ),
      body: StreamBuilder(
        stream: FirebaseDatabase.instance
            .reference()
            .child('notifications')
            .child(userId)
            .onValue,
        builder: (ctx, notificationsSnapshot) {
          if (notificationsSnapshot.connectionState ==
              ConnectionState.waiting) {
            return Center(
              child: CircularProgressIndicator(),
            );
          }
          if (notificationsSnapshot.hasData &&
              notificationsSnapshot.data.snapshot.value != null) {
            Map data = notificationsSnapshot.data.snapshot.value;

            List<Notifications> notificationList = [];
            data.forEach((key, data) {
              notificationList.add(
                Notifications(
                  content: data['content'],
                  dateTime: DateTime.parse(data['time']),
                ),
              );
            });
            notificationList.sort(
              (a, b) => b.dateTime.compareTo(a.dateTime),
            );

            if (notificationList.length == 0) {
              return Center(
                child: Text('no notifications yet!!'),
              );
            }
            return ListView.builder(
                itemCount: notificationList.length,
                itemBuilder: (ctx, index) {
                  final notificationDate = serviceProvider.convertDatePreview(
                    notificationList[index].dateTime,
                  );
                  return Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      SizedBox(height: 7),
                      ListTile(
                        leading: Icon(Icons.notifications_none),
                        title: Text(
                          notificationList[index].content,
                        ),
                        trailing: Text(notificationDate),
                      ),
                      Divider(),
                    ],
                  );
                });
          }
          return Center(
            child: Text('no notifications yet....'),
          );
        },
      ),
    );
  }
}
