import 'package:flutter/material.dart';

import './register-screen.dart';
import './terms-screen.dart';

class WelcomeScreen extends StatefulWidget {
  static const routeName = '/welcome-screen';

  @override
  _WelcomeScreenState createState() => _WelcomeScreenState();
}

class _WelcomeScreenState extends State<WelcomeScreen> {
  var _checkValue = false;
  @override
  Widget build(BuildContext context) {
    final mediaQuery = MediaQuery.of(context).size.height;

    return Scaffold(
      body: Container(
        width: double.infinity,
        height: double.infinity,
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage('assets/images/BG.png'),
            fit: BoxFit.cover,
          ),
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            SizedBox(
              height: mediaQuery * .1,
            ),
            // this contianer contians GIF image for welcoming
            Container(
              height: mediaQuery * .3,
              child: Image(
                image: AssetImage('assets/images/Hi.png'),
              ),
            ),
            SizedBox(height: mediaQuery * .1),
            // this container contains welcoming text
            Container(
              height: mediaQuery * .1,
              child: Text(
                'Welcome to Grupo!',
                style: TextStyle(
                  color: Color(0xffffcaa6),
                  fontSize: 18,
                ),
              ),
            ),
            SizedBox(
              height: mediaQuery * .2,
            ),
            // this container with white color opacity(.1) contains continue button and term of use checkbox
            Container(
              height: mediaQuery * .2,
              decoration: BoxDecoration(
                color: Colors.white.withOpacity(.1),
              ),
              width: double.infinity,
              child: Column(
                children: [
                  // this Row() contians "checkbox and i agree with term and the flat button navigating to Terms of use screen "
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Checkbox(
                        value: _checkValue,
                        onChanged: (value) {
                          setState(() {
                            _checkValue = value;
                          });
                        },
                      ),
                      Text(
                        'I agree to',
                        style: TextStyle(
                          fontSize: 15,
                          color: Colors.white,
                        ),
                      ),
                      InkWell(
                        child: Text(
                          ' Terms of Use',
                          style: TextStyle(
                            decoration: TextDecoration.underline,
                            color: Colors.white,
                            fontSize: 15,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        onTap: () {
                          Navigator.of(context)
                              .pushNamed(TermsScreen.routeName);
                        },
                      )
                    ],
                  ),
                  // container contains continue button
                  Container(
                    width: MediaQuery.of(context).size.width * .8,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(50),
                    ),
                    child: RaisedButton(
                      color: Color(0xffd75a57),
                      padding: const EdgeInsets.all(10),
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(30),
                      ),
                      onPressed: () {
                        if (_checkValue) {
                          Navigator.of(context)
                              .pushNamed(RegisterScreen.routeName);
                        }
                      },
                      child: Text(
                        'Continue',
                        style: TextStyle(fontSize: 20, color: Colors.white),
                      ),
                    ),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
