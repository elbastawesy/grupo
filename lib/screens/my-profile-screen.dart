import 'package:flutter/material.dart';
import 'package:grupo/model/user-model.dart';
import 'package:grupo/providers/sercvice_provider.dart';
import 'package:grupo/save_date_locally_by_sharedPreferences.dart';

import 'package:intl/intl.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:provider/provider.dart';

import '../widgets/profile_user_name_and_avatar_widget.dart';
import '../widgets/alert-dialog-widget.dart';
import '../constants/messages.dart';
import './register-screen.dart';

class MyProfileScreen extends StatefulWidget {
  @override
  _MyProfileScreenState createState() => _MyProfileScreenState();
}

class _MyProfileScreenState extends State<MyProfileScreen> {
  final userId = FirebaseAuth.instance.currentUser.uid;
  final appBar = AppBar(
    title: Text('Profile'),
    centerTitle: true,
    leading: FlatButton(
      child: null,
      onPressed: null,
    ),
  );
  bool _fetchingData = false;
  GrupoUser user = GrupoUser(
    userName: "",
    userNumber: "",
    avatar: '',
    online: true,
    deactivated: false,
    longitude: 0.0,
    latitude: 0.0,
    id: '',
    range: 1,
    joinDate: DateTime.now(),
  );

  void _fetchUserData() async {
    try {
      final userData =
          Provider.of<ServiceProvider>(context, listen: false).userInformations;
      user = GrupoUser(
        userName: userData.userName,
        userNumber: userData.userNumber,
        avatar: userData.avatar,
        range: userData.range,
        id: userData.id,
        latitude: userData.latitude,
        longitude: userData.longitude,
        deactivated: userData.deactivated,
        online: userData.online,
        joinDate: userData.joinDate,
      );
      setState(() {
        _fetchingData = false;
      });
    } catch (e) {
      print('the error is : $e');
      AlertDialogWidget().myAlert(context, PROFILE_ERROR_MESSAGE);
    }
  }

  @override
  void initState() {
    super.initState();
    _fetchingData = true;
    _fetchUserData();
  }

  @override
  Widget build(BuildContext context) {
    final mediaQueryHeight = MediaQuery.of(context).size.height -
        appBar.preferredSize.height -
        MediaQuery.of(context).padding.top;
    final _convertedJoinDate = DateFormat(' MMM. y').format(user.joinDate);
    return Scaffold(
      appBar: appBar,
      body: _fetchingData
          ? Center(
              child: CircularProgressIndicator(
                backgroundColor: Theme.of(context).primaryColor,
              ),
            )
          : SingleChildScrollView(
              child: Column(
                children: [
                  UserNameAndUserAvatarWidget(
                    userName: user.userName,
                    userAvatar: user.avatar,
                    mediaQueryHeight: mediaQueryHeight * .5,
                  ),
                  Container(
                    height: mediaQueryHeight * .4,
                    padding: EdgeInsets.only(
                      left: MediaQuery.of(context).size.width * .05,
                      top: mediaQueryHeight * .02,
                    ),
                    alignment: Alignment.centerLeft,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          'Account Created Since $_convertedJoinDate',
                          style: TextStyle(fontSize: 16),
                        ),
                        SizedBox(height: mediaQueryHeight * .02),
                        Text('Phone number', style: TextStyle(fontSize: 25)),
                        SizedBox(height: mediaQueryHeight * .01),
                        Row(
                          children: [
                            Text(user.userNumber,
                                style: TextStyle(fontSize: 16)),
                            SizedBox(
                                width: MediaQuery.of(context).size.width * .02),
                            Icon(
                              Icons.lock_sharp,
                              size: 16,
                            ),
                          ],
                        ),
                        SizedBox(
                          height: mediaQueryHeight * .05,
                        ),
                        Divider(
                          color: Colors.grey,
                        ),
                        FlatButton.icon(
                          icon: Icon(
                            Icons.clear_outlined,
                            size: 25,
                            color: Colors.red,
                          ),
                          label: Text(
                            'De-activate Account',
                            style: TextStyle(
                              color: Colors.red,
                              fontSize: 25,
                            ),
                          ),
                          onPressed: () {
                            onDeactivating(context);
                          },
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
    );
  }

  onDeactivating(BuildContext context) async {
    showDialog(
        context: context,
        builder: (ctx) {
          return AlertDialog(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(20),
            ),
            content: Text(
              DE_ACTIVATE_MY_ACCOUNT_CONFIRM_MESSAGE,
              style: TextStyle(
                color: Color(0xff2c0817),
                fontSize: 14,
              ),
            ),
            actions: [
              FlatButton(
                onPressed: () async {
                  // await Provider.of<ServiceProvider>(
                  //   context,
                  //   listen: false,
                  // ).deactivateAccount(userId, context);

                  await SaveDataLocally().removeUserData();
                  Navigator.of(context)
                      .pushReplacementNamed(RegisterScreen.routeName);
                },
                child: Text(
                  'Yes',
                  style: TextStyle(color: Color(0xff2c0817), fontSize: 18),
                ),
              ),
              FlatButton(
                onPressed: () {
                  Navigator.of(context).pop();
                },
                child: Text(
                  'No',
                  style: TextStyle(color: Color(0xff2c0817), fontSize: 18),
                ),
              ),
            ],
          );
        });
  }
}
