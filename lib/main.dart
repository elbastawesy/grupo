import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:grupo/providers/chat_provider.dart';
import 'package:grupo/screens/invite_friends_screen.dart';
import 'package:grupo/splash-screen.dart';
import 'package:provider/provider.dart';

import 'package:grupo/providers/sercvice_provider.dart';
import 'providers/favorite_messages_provider.dart';

import './screens/register-screen.dart';
import './screens/terms-screen.dart';
import './screens/welcome-screen.dart';
import './screens/choose-range-screen.dart';
import './screens/choose-avatar-screen.dart';
import './screens/main-screen.dart';
import './screens/grupo-chat-screen.dart';
import './screens/notifications-screen.dart';
import './screens/privacy_policy_screen.dart';
import './providers/notification-provider.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  FirebaseDatabase.instance.setPersistenceEnabled(true);
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations(
      [
        DeviceOrientation.portraitUp,
        DeviceOrientation.portraitDown,
      ],
    );
    return MultiProvider(
      providers: [
        ChangeNotifierProvider.value(
          value: ServiceProvider(),
        ),
        ChangeNotifierProvider.value(
          value: ChatProvider(),
        ),
        ChangeNotifierProvider.value(
          value: FavoriteMessagesProvider(),
        ),
        ChangeNotifierProvider.value(
          value: NotificationProvider(),
        ),
      ],
      child: MaterialApp(
          title: 'Grupo',
          theme: ThemeData(
            primaryColor: Color(0xffd75a56),
            accentColor: Color(0xffd75a55),
          ),
          debugShowCheckedModeBanner: false,
          home: SplashScreen(),
          routes: {
            TermsScreen.routeName: (ctx) => TermsScreen(),
            WelcomeScreen.routeName: (ctx) => WelcomeScreen(),
            RegisterScreen.routeName: (ctx) => RegisterScreen(),
            ChooseAvatarScreen.routeName: (ctx) => ChooseAvatarScreen(),
            ChooseRangeScreen.routeName: (ctx) => ChooseRangeScreen(),
            MainScreen.routeName: (ctx) => MainScreen(),
            NotificationsScreen.routName: (ctx) => NotificationsScreen(),
            GrupoChatScreen.routeName: (ctx) => GrupoChatScreen(),
            PrivacyPolicyScreen.routeName: (ctx) => PrivacyPolicyScreen(),
            InviteFriendsScreen.routeName: (ctx) => InviteFriendsScreen(),
          }),
    );
  }
}
